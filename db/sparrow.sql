-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 27, 2021 at 03:04 PM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sparrow`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `language` varchar(220) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `address` text,
  `img` varchar(255) DEFAULT NULL,
  `category_type` varchar(255) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `description` text,
  `status` int(10) DEFAULT NULL,
  `num_of_staff` varchar(60) DEFAULT NULL,
  `block` varchar(60) DEFAULT NULL,
  `google_embed` text,
  `deleted` int(10) DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `unpublish_date` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `language`, `phone`, `email`, `address`, `img`, `category_type`, `user_id`, `description`, `status`, `num_of_staff`, `block`, `google_embed`, `deleted`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`) VALUES
(2, 0, 'hello', NULL, NULL, NULL, NULL, NULL, 'category', 9, '', 1, NULL, 'non', NULL, NULL, '0000-00-00', '0000-00-00', '2021-02-26', NULL),
(6, NULL, 'hh', NULL, NULL, NULL, NULL, NULL, 'category', NULL, NULL, 1, NULL, 'non', NULL, NULL, NULL, NULL, '2021-03-08', NULL),
(8, NULL, 'sd', NULL, NULL, NULL, NULL, NULL, 'category', NULL, NULL, 1, NULL, 'about_us', NULL, NULL, NULL, NULL, '2021-03-09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `show_banner` int(11) NOT NULL,
  `link` varchar(250) DEFAULT NULL,
  `language` varchar(220) DEFAULT NULL,
  `menu_type_id` int(11) DEFAULT NULL,
  `ordering` float(10,2) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `modul_class` varchar(255) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `parent_id`, `show_banner`, `link`, `language`, `menu_type_id`, `ordering`, `image`, `status`, `user_id`, `modul_class`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'Home', 0, 0, 'home', NULL, NULL, NULL, NULL, 1, 9, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2021-02-26', '2021-04-07', 1),
(2, 'Services & Products', 0, 1, 'services', NULL, NULL, 1.00, NULL, 1, 9, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2021-03-03', '2021-04-07', 1),
(5, 'Contact Us', 0, 1, 'contact_us', NULL, NULL, 1.00, NULL, 1, NULL, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2021-03-03', '2021-03-31', 1),
(4, 'About Us', 0, 1, 'about_us', NULL, NULL, 1.00, NULL, 1, NULL, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2021-03-19', '2021-04-05', 1),
(3, 'News', 0, 1, 'product', NULL, NULL, 1.00, NULL, 1, NULL, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2021-03-26', '2021-04-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_post`
--

DROP TABLE IF EXISTS `menu_post`;
CREATE TABLE IF NOT EXISTS `menu_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `position` varchar(220) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=212 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_post`
--

INSERT INTO `menu_post` (`id`, `post_id`, `menu_id`, `position`) VALUES
(173, 7, 8, 'position_1'),
(172, 7, 8, 'position_1'),
(211, 31, 1, 'position_4'),
(210, 8, 1, 'position_7'),
(209, 6, 1, 'position_6'),
(208, 3, 1, 'position_5'),
(207, 5, 1, 'position_3'),
(206, 2, 1, 'position_2'),
(205, 7, 1, 'position_1'),
(186, 10, 5, 'position_1'),
(191, 11, 4, 'position_1'),
(192, 25, 3, 'position_1'),
(197, 9, 2, 'position_1');

-- --------------------------------------------------------

--
-- Table structure for table `menu_types`
--

DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE IF NOT EXISTS `menu_types` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_types`
--

INSERT INTO `menu_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(5, 'Footer', '2021-03-09', NULL),
(4, 'Header', '2021-03-08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(220) DEFAULT NULL,
  `description` varchar(220) DEFAULT NULL,
  `image` varchar(220) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `link` varchar(250) DEFAULT NULL,
  `action_title` varchar(220) DEFAULT NULL,
  `show_image_feature` int(11) DEFAULT '1',
  `image` varchar(250) DEFAULT NULL,
  `description` text,
  `template` varchar(200) DEFAULT NULL,
  `count` int(5) DEFAULT NULL,
  `language` varchar(220) DEFAULT NULL,
  `link_download` varchar(400) DEFAULT NULL,
  `post_type` varchar(200) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `is_againt` int(11) DEFAULT NULL,
  `deleted` int(10) DEFAULT NULL,
  `event_start_date` datetime DEFAULT NULL,
  `event_end_date` datetime DEFAULT NULL,
  `s_from_rang` float DEFAULT NULL,
  `s_to_rang` float DEFAULT NULL,
  `location_job` varchar(60) DEFAULT NULL,
  `job_type` varchar(60) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `link`, `action_title`, `show_image_feature`, `image`, `description`, `template`, `count`, `language`, `link_download`, `post_type`, `user_id`, `status`, `is_againt`, `deleted`, `event_start_date`, `event_end_date`, `s_from_rang`, `s_to_rang`, `location_job`, `job_type`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`) VALUES
(7, 'Slide', NULL, '1', 1, NULL, 'Welcome to sparrow pos', 'slide', NULL, NULL, NULL, 'multi', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-03-23', '2021-04-06'),
(2, 'What we do for you', NULL, '1', 1, NULL, 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore smod tem magna aliqua. Ut enim.', 'multi_post_title', NULL, NULL, NULL, 'multi', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-03-23', '2021-04-07'),
(3, 'Completed Cases', NULL, '1', 1, NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali quUt enim ad minim veniam. quis nostrud exercitation.', 'multi_post', NULL, NULL, NULL, 'multi', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-03-23', '2021-04-07'),
(5, 'Work from anywhere on multiple devices', NULL, '1', 1, 'depositphotos_158349312-stock-photo-point-of-sale-system-for.jpg', '<p>Access and manage your books from your computer, \r\nlaptop, tablet, or smartphone anytime you choose. \r\nCreate access privileges so that your colleague or \r\naccountant can login and work with your data online.</p>', 'single_post', NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-03-23', '2021-03-31'),
(6, 'Slick Slider', NULL, '0', 0, NULL, NULL, 'slick_slider', NULL, NULL, NULL, 'multi', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-03-23', '2021-04-07'),
(8, 'Request For Call Back', NULL, '1', 0, NULL, 'Leave your messages here, We\'ll give you a call later', 'request', NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-25 11:13:00', '2021-03-25 12:00:00', '2021-03-25', '2021-04-16'),
(9, 'Subscription', NULL, '1', 1, '', '<table class=\"table table-striped\" style=\"text-align: left;\"><tbody><tr><td style=\"text-align: center; \">Features</td><td style=\"text-align: center; \">Local POS&nbsp;</td><td style=\"text-align: center; \">Hosted Lite</td><td style=\"text-align: center; \">Hosted Premium</td></tr><tr><td style=\"text-align: left; \">Support multiple devices like computer, MacBook, iPad, phone, and tablet</td><td style=\"text-align: center; \"><font color=\"#000000\" face=\"Muli, sans-serif\">√</font></td><td style=\"text-align: center; \">√</td><td style=\"text-align: center; \">√</td></tr><tr><td style=\"text-align: left; \">Excel import &amp; export capability</td><td style=\"text-align: center; \">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left; \">Sales interface</td><td style=\"text-align: center; \">√</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Multiple sales order before check out</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Gift card transaction capability</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Lots of reporting</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Add and track items easily</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Graphical reporting</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Track employee commissions</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Manage inventory &amp; process sales</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Multiple employees</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Bar code scanner &amp; receipt printer Capability</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Store configuration</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Multiple language capability&nbsp;</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Multiple currencies</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Multiple locations</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Customer tracking</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Sales tax tracking</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Accessibility</td><td style=\"text-align: center;\">Locally</td><td style=\"text-align: center;\">Hosted</td><td style=\"text-align: center;\">Hosted</td></tr><tr><td style=\"text-align: left;\">Source code modification</td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td></tr><tr><td style=\"text-align: left;\">Free upgrades</td><td style=\"text-align: center;\">6 months</td><td style=\"text-align: center;\">Always</td><td style=\"text-align: center;\">Always</td></tr><tr><td style=\"text-align: left;\">Automatic backups every night</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Firewall (security)&nbsp;</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Auto-Renew&nbsp;</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">√</td><td style=\"text-align: center;\">√</td></tr><tr><td style=\"text-align: left;\">Expiration</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">Upon Cancellation</td><td style=\"text-align: center;\">Upon Cancellation</td></tr><tr><td style=\"text-align: left;\">Disk space</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">5 GB</td><td style=\"text-align: center;\">50 GB</td></tr><tr><td style=\"text-align: left;\">Bandwidth&nbsp;</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">50 GB</td><td style=\"text-align: center;\">500 GB</td></tr><tr><td style=\"text-align: left;\">International uplink/downlink</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">1mbps</td><td style=\"text-align: center;\">5mbps</td></tr><tr><td style=\"text-align: left;\">Local uplink/downlink</td><td style=\"text-align: center;\"><font color=\"#ff0000\">x</font></td><td style=\"text-align: center;\">30mbps</td><td style=\"text-align: center;\">50mbps</td></tr><tr><td style=\"text-align: center;\"><p style=\"text-align: left; \">Technical support</p><p style=\"text-align: left; \">E-Mail (All Countries)</p><p style=\"text-align: left; \">Phone (Cambodia)&nbsp;</p></td><td style=\"text-align: center;\"><p>$5 per hour</p><p>(Free 10 hours after </p><p>\r\nfirst purchase)</p></td><td style=\"text-align: center;\"><p>$5 per hour</p><p>(Free 5 hours after</p><p>every month)</p></td><td style=\"text-align: center;\"><p>$5 per hour</p><p>(Free 10 hours </p><p>every month)</p></td></tr><tr><td style=\"text-align: left;\">Price</td><td style=\"text-align: center;\"><b>$250.00</b></td><td style=\"text-align: center;\"><b>$25.00</b></td><td style=\"text-align: center;\"><b>$25.00</b></td></tr></tbody></table><p><br></p>', 'subscription', NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-03-31', '2021-03-31'),
(10, 'Contact Us', NULL, '1', 1, '', NULL, 'contact', NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-03-31', '2021-03-31'),
(11, 'Why Choose Sparrow POS ?', NULL, '1', 1, 'thxij8a9vdniupu6locy.jpg', '<ul><li>Save Times</li><li>Increase Sales</li><li>Make Informed Decisions</li><li>Measurable Results</li><li>Communicate more effectively with customers, supplier or partners</li><li>Global Access 24 hours a day, 7 days a week</li><li>Access and manage from your computer, laptop, tablet, or smartphone anytime you choose</li><li>Create Access Privileges so that your colleague or accountant can login and work with your data online</li></ul>', 'about_us', NULL, NULL, NULL, 'multi', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-02', '2021-04-08'),
(29, 'Slick slider 1', NULL, '1', 1, 'logo.jpg', 'You can’t succeed if you just do what others do and follow the well-worn path. You need to create a new and original path for yourself.', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(30, 'Slick slider 2', NULL, '1', 1, 'logo.jpg', 'You can’t succeed if you just do what others do and follow the well-worn path. You need to create a new and original path for yourself.', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(32, 'SPARROW POS', NULL, '1', 1, '', 'We pride ourselves on providing a great service, with great support in 2018, was founded to bring business owners from all across the spectrum the technology they need to increase sales, make better business decisions, increase the Number of happier customers, and streamline their business. \r\nWe have shopped, compared, and tested the leading POS solutions and complementary technology, and have collected the best of breed to provide to you the best POS solution for your business. We’ve also done development of our own, in order to deliver the Sparrow POS System, you need - at the price you want.\r\nWhether you are opening your first store, restaurant, or salon, you will be able to find the right Sparrow POS solution for your business. we offer the solutions to your problems that include software, hardware, and most importantly service throughout all the steps of the process, from installation, training, integrations, and post support.', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-08', '2021-04-08'),
(17, 'We will help you to grow your business', NULL, '1', 1, 'h1_hero.jpg', 'welcome to sparrow pos', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-06', '2021-04-06'),
(18, 'Business Planning', NULL, '1', 1, 'team_1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore .', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(19, 'Final Projection', NULL, '1', 1, 'team_2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore .', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(16, 'Start Your Business From Today', NULL, '1', 1, 'banner.jpg', 'welcome to sparrow pos', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-06', '2021-04-06'),
(20, 'Marketing Strategy', NULL, '1', 1, 'completed_case_1.png', 'Completely impact synergistic mindshare whereas premium services.', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(21, 'Marketing Strategy', NULL, '1', 1, 'completed_case_2.png', 'Completely impact synergistic mindshare whereas premium services.', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(31, 'Sparrow Benefit', NULL, '1', 1, '', NULL, 'post_detail', NULL, NULL, NULL, 'multi', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(22, 'Save Time', NULL, '1', 1, 'clock.png', '2-3 hours of manual work \r\na week that our system \r\nautomates', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(23, 'Increase Sales', NULL, '1', 1, 'trend.png', 'Our user-friendly system speeds up checkout and increases for your business', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(24, 'Make Informed Decisions', NULL, '1', 1, 'test.png', 'Order the right products \r\nfor your store using our \r\nreporting', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(25, 'Recent News', NULL, '1', 1, NULL, NULL, 'recent_news', NULL, NULL, NULL, 'multi', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(26, 'Amazing Stock Management', NULL, '1', 1, 'rcent_1.png', 'Business Planning', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(27, 'Provide Services for 24 hour', NULL, '1', 1, 'rcent_2.png', 'POS Technical Support', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07'),
(28, 'Discuss About Your Business', NULL, '1', 1, 'rcent_3.png', 'Flow of working', NULL, NULL, NULL, NULL, 'single', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 12:00:00', '1970-01-01 12:00:00', '2021-04-07', '2021-04-07');

-- --------------------------------------------------------

--
-- Table structure for table `post_image`
--

DROP TABLE IF EXISTS `post_image`;
CREATE TABLE IF NOT EXISTS `post_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_image`
--

INSERT INTO `post_image` (`id`, `post_id`, `image`) VALUES
(1, NULL, 'abstract-company-logo-E53BA09D43-seeklogo.com (2).png'),
(2, NULL, 'abstract-company-logo-E53BA09D43-seeklogo.com.png'),
(16, 13, 'team_1.jpg'),
(17, 13, 'team_1.jpg'),
(18, 14, 'team_2.jpg'),
(23, 7, 'h1_hero.jpg'),
(24, 7, 'LS-One-for-restaurants-main-header.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `website_name` varchar(250) NOT NULL,
  `website_url` varchar(250) DEFAULT NULL,
  `language` varchar(220) DEFAULT NULL,
  `address` text,
  `phone` text,
  `email` text,
  `logo_image` varchar(250) DEFAULT NULL,
  `logo_text` varchar(250) DEFAULT NULL,
  `favicon_image` varchar(250) DEFAULT NULL,
  `copyright` varchar(200) DEFAULT NULL,
  `work_time` varchar(220) DEFAULT NULL,
  `address_site` text,
  `is_slide_only_page` int(11) DEFAULT NULL,
  `link_fb` text,
  `user_id` int(5) DEFAULT NULL,
  `google_map` text,
  `facebooklink` text,
  `visitor_counter` text,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `website_name`, `website_url`, `language`, `address`, `phone`, `email`, `logo_image`, `logo_text`, `favicon_image`, `copyright`, `work_time`, `address_site`, `is_slide_only_page`, `link_fb`, `user_id`, `google_map`, `facebooklink`, `visitor_counter`, `created_at`, `updated_at`) VALUES
(3, 'Get in Touch', 'www.sparrow.com', 'kh', 'Room 401, 4th Floor, Mekong View Tower 1, Chroy Changva, Phnom Penh, Cambodia', '11 22 33 44', 'info@titb.com', NULL, NULL, NULL, '<p><br></p>', '<p><br></p>', '<p><br></p>', 0, NULL, NULL, NULL, NULL, NULL, '2021-04-16', '2021-04-16');

-- --------------------------------------------------------

--
-- Table structure for table `sub_post`
--

DROP TABLE IF EXISTS `sub_post`;
CREATE TABLE IF NOT EXISTS `sub_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `sub_post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_post`
--

INSERT INTO `sub_post` (`id`, `post_id`, `sub_post_id`) VALUES
(24, 12, 7),
(37, 2, 18),
(23, 12, 6),
(30, 7, 16),
(36, 2, 19),
(38, 3, 20),
(31, 7, 17),
(39, 3, 21),
(40, 4, 22),
(41, 4, 23),
(42, 4, 24),
(43, 25, 26),
(44, 25, 27),
(45, 25, 28),
(46, 6, 29),
(47, 6, 30),
(48, 31, 22),
(49, 31, 23),
(50, 31, 24),
(51, 11, 32);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `address` text,
  `status` int(10) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `remember_token` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `phone`, `image`, `branch_id`, `address`, `status`, `password`, `created_at`, `updated_at`, `remember_token`) VALUES
(18, 'Sokchea', 'Sokchea Leang', 'sokchea@titb.com', '016920587', '', NULL, NULL, 1, '$2y$10$NKdqSiiSOFh2AKf1WKR.i.0M7jFf2edAJ9tfAzB3ID0eoO1yV4ziS', NULL, NULL, NULL),
(19, 'phearom', 'Phearom Phon', 'phearom@titb.com', NULL, '', NULL, NULL, 1, '$2y$10$RIu4ovLpSrdmPIIHnasC4eJXBYl1OYzCAXDLvBtD2jEZcqHPJp922', NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
