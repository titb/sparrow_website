<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Sparrow Point of Sale</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
		<link rel="shortcut icon" type="image/x-icon" href="{{url('assets/img/logo/logo.jpg')}}">
		<!-- CSS here -->
        <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/flaticon.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/slicknav.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/animate.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/fontawesome-all.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/themify-icons.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/slick.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/nice-select.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
        <link rel="stylesheet" href="{{url('css/style.css')}}">
        <link rel="stylesheet" href="{{url('css/header.css')}}">
        <link rel="stylesheet" href="{{url('css/contact_us.css')}}">
        <link rel="stylesheet" href="{{url('css/footer.css')}}">

   </head>
   <body>
   <!-- <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{url('assets/img/logo/logo.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </div> -->
    <!-- Preloader Start -->
    <header>
        <!-- Header Start -->
       <div class="header-area">
            <div class="main-header ">
                <div class="header-top top-bg  d-lg-block">
                   <div class="container-fluid">
                       <div class="col-xl-12">
                            <div class="row d-flex justify-content-between align-items-center">
                                <div class="header-info-left">
                                    <ul>     
                                        <!-- <li><i class="fas fa-map-marker-alt"></i>Room 401, 4th Floor, Mekong View Tower 1, Chroy Changva, Phnom Penh, Cambodia</li> -->
                                    </ul>
                                </div>
                                <div class="header-info-right">
                                    <ul class="header-social">    
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="https://www.facebook.com/sparrowpos"><i class="fab fa-facebook-f"></i></a></li>
                                       <li> <a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                       <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">English</a>
                                            <div class="dropdown-menu" aria-labelledby="dropdown09">
                                                <a class="dropdown-item" style="color:#002e5b;" href="#fr"> French</a>
                                                <a class="dropdown-item" style="color:#002e5b;" href="#it">Italian</a>
                                                <a class="dropdown-item" style="color:#002e5b;" href="#ru">Russian</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                       </div>
                   </div>
                </div>
               <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-4 col-lg-1 col-md-1">
                                <div class="logo">
                                  <a href="index.html">
                                    <img src="{{url('assets/img/logo/logo.jpg')}}" alt="">
                                  </a>
                                </div>
                            </div>
                            <div class="col-xl-8 col-lg-11 col-md-11">
                                <!-- Main-menu -->
                                <div class="main-menu f-right d-none d-lg-block">
                                    <nav> 
                                        <ul id="navigation">
                                        @foreach($Menu as $m)
                                        <?php
                                            $submenu=App\Models\Menu::where('parent_id','=',$m->id)->get();                       
                                        ?> 
                                            @if(count($submenu)>0)
                                                <li><a href="{{$m->link}}">{{$m->name}}</a>
                                                    <ul class="submenu">
                                                        @foreach($submenu as $s)
                                                        <li><a href="{{$s->link}}">{{$s->name}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li><a href="{{$m->link}}" class="{{ Request::segment(1)==$m->link ? 'active':'' }}">{{$m->name}}</a></li>
                                            @endif
                                        @endforeach
                                        </ul>
                                    </nav>
                                </div>
                            </div>             
                            <!-- <div class="col-xl-2 col-lg-3 col-md-3">

                            </div> -->
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
       </div>
    
        <!-- Header End -->
    </header>
    <main>
    @if($menu->show_banner==1)
    @include('banner')
    @endif
    @yield('content')
    </main>
   <footer>
       <!-- Footer Start-->
       <div class="footer-area footer-padding">
           <div class="container">
               <div class="row d-flex justify-content-between">
                   <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                      <div class="single-footer-caption mb-50">
                        <div class="single-footer-caption mb-30">
                             <!-- logo -->
                            <div class="footer-logo">
                            <h4>Trusted IT Business (TITB)</h4>
                                <!-- <a href="index.html"><img src="{{url('assets/img/logo/logo2_footer.png')}}" alt=""></a> -->
                            </div>
                            <div class="footer-tittle">
                                <div class="footer-pera">
                                    <p>Accepted Payments</p>
                               </div>
                            </div>
                            <!-- social -->
                            <div class="footer-social">
                                <img src="{{url('assets/img/icon/titb_pay.png')}}" alt="">
                                <img src="{{url('assets/img/icon/titb_payza.png')}}" alt="">
                                <img src="{{url('assets/img/icon/titb_pm.png')}}" alt="">
                                <img src="{{url('assets/img/icon/titb_anz.png')}}" alt="">
                                <img src="{{url('assets/img/icon/titb_wing.png')}}" alt="">
                                <br>
                                <img src="{{url('assets/img/icon/aba.png')}}" alt="">
                                <img src="{{url('assets/img/icon/titb_skrill.png')}}" alt="">
                                <img src="{{url('assets/img/icon/titb_wire.png')}}" alt="">
                                <img src="{{url('assets/img/icon/titb_aceleda.png')}}" alt="">
                            </div>
                        </div>
                      </div>
                   </div>
                   <div class="col-xl-2 col-lg-2 col-md-4 col-sm-5">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>Company</h4>
                               
                               <ul>
                               @foreach($Menu as $m)
                                   <li><a href="{{$m->link}}">{{ $m->name }}</a></li>
                                @endforeach
                               </ul>
                               
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-4 col-sm-7">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>Services</h4>
                               <ul>
                                   <li><a href="#">Pos System</a></li>
                                   <li><a href="#">Website Development</a></li>
                                   <li><a href="#">Domain Name</a></li>
                                   <li><a href="#">Hosting Plan</a></li>
                                   <li><a href="#">Technical Support</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>{{ $setting->website_name }}</h4>
                               <ul>
                               
                                <li><a href="#">{{ $setting->phone }}</a></li>
                                <li><a href="#">{{ $setting->email }}</a></li>
                                <li><a href="#">{{ $setting->address }}</a></li>
                                
                            </ul>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <!-- footer-bottom aera -->
       <div class="footer-bottom-area footer-bg">
           <div class="container">
               <div class="footer-border">
                    <div class="row d-flex align-items-center">
                        <div class="col-xl-12 ">
                            <div class="footer-copy-right text-center">
                                <p>
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script>- titb.biz - All Rights Reserved.
                                </p>
                            </div>
                        </div>
                    </div>
               </div>
           </div>
       </div>
       <!-- Footer End-->
   </footer>
   
	<!-- JS here -->
	
		<!-- All JS Custom Plugins Link Here here -->
        <script src="{{url('./assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
		<!-- Jquery, Popper, Bootstrap -->
		<script src="{{url('./assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{url('./assets/js/popper.min.js')}}"></script>
        <script src="{{url('./assets/js/bootstrap.min.js')}}"></script>
	    <!-- Jquery Mobile Menu -->
        <script src="{{url('./assets/js/jquery.slicknav.min.js')}}"></script>

		<!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="{{url('./assets/js/owl.carousel.min.js')}}"></script>
        <script src="{{url('./assets/js/slick.min.js')}}"></script>
        <!-- Date Picker -->
        <script src="{{url('./assets/js/gijgo.min.js')}}"></script>
		<!-- One Page, Animated-HeadLin -->
        <script src="{{url('./assets/js/wow.min.js')}}"></script>
		<script src="{{url('./assets/js/animated.headline.js')}}"></script>
        <script src="{{url('./assets/js/jquery.magnific-popup.js')}}"></script>

		<!-- Scrollup, nice-select, sticky -->
        <script src="{{url('./assets/js/jquery.scrollUp.min.js')}}"></script>
        <script src="{{url('./assets/js/jquery.nice-select.min.js')}}"></script>
		<script src="{{url('./assets/js/jquery.sticky.js')}}"></script>
        
        <!-- contact js -->
        <script src="{{url('./assets/js/contact.js')}}"></script>
        <script src="{{url('./assets/js/jquery.form.js')}}"></script>
        <script src="{{url('./assets/js/jquery.validate.min.js')}}"></script>
        <script src="{{url('./assets/js/mail-script.js')}}"></script>
        <script src="{{url('/assets/js/jquery.ajaxchimp.min.js')}}."></script>
        
		<!-- Jquery Plugins, main Jquery -->	
        <script src="{{url('./assets/js/plugins.js')}}"></script>
        <script src="{{url('./assets/js/main.js')}}"></script>
    </body>
</html>