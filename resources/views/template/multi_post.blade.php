<div class="completed-cases section-padding3">
    <div class="container-fluid">
        <div class="row">
            <!-- slider Heading -->
            <div class="col-xl-4 col-lg-4 col-md-8">
                <div class="single-cases-info mb-30">
                    <h3>{{ $p->title }}</h3>
                    <p>{!! $p->description !!}</p>
                    <a href="gallery.html" class="border-btn border-btn2">See more</a>
                </div>
            </div>
            <!-- OwL -->
            <div class="col-xl-8 col-lg-8 col-md-col-md-7">
                <div class=" completed-active owl-carousel">
                @foreach($p->sub_posts as $sub)
                    <div class="single-cases-img">
                        <img src="{{url('images/'.$sub->image)}}" alt="">
                        <!-- img hover caption -->
                        <div class="single-cases-cap">
                            <h4><a href="case_details.html">{{ $sub->title }}</a></h4>
                            <p>{!! $sub->description !!}</p>
                            <span>Advisory</span>
                        </div>
                    </div>
                @endforeach
                    <!-- <div class="single-cases-img">
                        <img src="{{url('assets/img/service/completed_case_2.png')}}" alt=""> 
                        <div class="single-cases-cap">
                            <h4><a href="case_details.html">Marketing Strategy</a></h4>
                            <p>Completely impact synergistic mindshare whereas premium services.</p>
                            <span>Advisory</span>
                        </div>
                    </div> -->
                </div>                
            </div>
        </div>
    </div>
</div>