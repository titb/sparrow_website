
<div class="slider-area ">
         <!-- Mobile Menu -->
    <div class="slider-active">
        @if($p->image)
            @foreach($p->image_post as $i)   
                <div class="single-slider slider-height d-flex align-items-center" data-background="{{url('Galleries/'.$i->image)}}">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-8">
                                <div class="hero__caption">
                                    <p data-animation="fadeInLeft" data-delay=".4s">{!! $p->description !!}</p>
                                    <h1 data-animation="fadeInLeft" data-delay=".6s" >{{ $p->title }}</h1>
                                    <div class="hero__btn" data-animation="fadeInLeft" data-delay=".8s">
                                        <a href="industries.html" class="btn hero-btn">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        @if($p->sub_posts)
            @foreach($p->sub_posts as $sub)
                    <div class="single-slider slider-height d-flex align-items-center" data-background="{{url('images/'.$sub->image)}}">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-8">
                                    <div class="hero__caption">
                                        <p data-animation="fadeInLeft" data-delay=".4s">{!! $sub->description !!}</p>
                                        <h1 data-animation="fadeInLeft" data-delay=".6s" >{{ $sub->title }}</h1>
                                        <div class="hero__btn" data-animation="fadeInLeft" data-delay=".8s">
                                            <a href="industries.html" class="btn hero-btn">Learn More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            @endif
    </div>
</div>