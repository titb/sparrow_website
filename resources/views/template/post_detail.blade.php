<div class="services-area section-padding2">
            <div class="container">
                <!-- section tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center">
                            <h2>{{ $p->title}}</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                @foreach($p->sub_posts as $sub)
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-icon">
                                <img src="{{url('images/'.$sub->image)}}" alt="">
                            </div>
                            <div class="services-caption">
                                <h4>{{ $sub->title}}</h4>
                                <p>{!! $sub->description !!}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
                <!-- button -->
                <div class="row justify-content-center">
                    <div class="room-btn pt-50">
                        <a href="services.html" class="border-btn">More Services</a>
                    </div>
                </div>
            </div>
        </div>