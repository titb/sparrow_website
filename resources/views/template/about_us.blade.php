<div class="container">
    @foreach($p->sub_posts as $sub)
    <div class="trusted-caption">
    <div class="center">
        <h2>{{ $sub->title}}</h2>
    </div>
        <p>{!! $sub->description !!}</p>
    </div>
    @endforeach
</div>
<div class="we-trusted-area about_padding">
    <div class="container">
        <div class="row d-flex align-items-end">
            <div class="col-xl-7 col-lg-7">
                <div class="trusted-img">
                    <img src="{{url('images/'.$p->image)}}" alt="">
                </div>
            </div>
            <div class="col-xl-5 col-lg-5">
                <div class="trusted-caption">
                    <h2>{{$p->title}}</h2>
                    <p>{!!$p->description!!}</p>
                </div>
            </div>
        </div>
    </div>
</div>

