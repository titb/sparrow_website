<div class="we-trusted-area trusted-padding">
    <div class="container">
        <div class="row d-flex align-items-end">
            <div class="col-xl-7 col-lg-7">
                <div class="trusted-img">
                    <img src="{{url('images/'.$p->image)}}" alt="">
                </div>
            </div>
            <div class="col-xl-5 col-lg-5">
                <div class="trusted-caption">
                    <h2>{{$p->title}}</h2>
                    <p>{!!$p->description!!}</p>
                    <a href="#" class="btn trusted-btn">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</div>