<div class="team-profile team-padding">
    <div class="container">
        <div class="row">
        @if($p->image_post)
            @foreach($p->image_post as $i)  
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-profile mb-30">
                    <div class="single-profile-front">
                        <div class="profile-img">
                            <img src="{{url('Galleries/'.$i->image)}}" alt="">
                        </div>
                        <div class="profile-caption">
                            <h4><a href="#">{{ $i->title }}</a></h4>
                            <p>{!! $i->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
        @if($p->sub_posts)
        @foreach($p->sub_posts as $sub)
        <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-profile mb-30">
                    <!-- Front -->
                    <div class="single-profile-front">
                        <div class="profile-img">
                            <img src="{{url('images/'.$sub->image)}}" alt="">
                        </div>
                        <div class="profile-caption">
                            <h4><a href="#">{{ $sub->title }}</a></h4>
                            <p>{!! $sub->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @endif
        <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="single-profile mb-30">
                <div class="single-profile-back-last">
                    <h2>{{ $p->title }}</h2>
                    <p>{!! $p->description !!}</p>
                    <a href="#">View profile »</a>
                </div>
            </div>
        </div>
            <!-- <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-profile mb-30">
                    <div class="single-profile-front">
                        <div class="profile-img">
                            <img src="{{url('assets/img/team/team_2.jpg')}}" alt="">
                        </div>
                        <div class="profile-caption">
                            <h4><a href="#">Financial Projections</a></h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore .</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-profile mb-30">
                    <div class="single-profile-back-last">
                        <h2>What we do for you</h2>
                        <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore smod tem magna aliqua. Ut enim.</p>
                        <a href="#">View profile »</a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>