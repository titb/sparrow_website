<div class="testimonial-area fix">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-9 col-md-9">
                <div class="h1-testimonial-active">
                    <!-- Single Testimonial -->
                @foreach($p->sub_posts as $sub)
                    <div class="single-testimonial pt-65">
                        <!-- Testimonial tittle -->
                        <div class="testimonial-icon mb-45">
                            <img src="{{url('images/'.$sub->image)}}" class="ani-btn " alt="">
                        </div>
                            <!-- Testimonial Content -->
                        <div class="testimonial-caption text-center">
                            <p>{!! $sub->description !!}</p>
                            <!-- Rattion -->
                            <div class="testimonial-ratting">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>