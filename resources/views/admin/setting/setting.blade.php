@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        <div class="title_left">
            <h3>Setting</h3>
        </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
            @include('errors.message_error')
            <a class="create" href="{{('setting/create_setting')}}">Create Setting</a>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <table id="datatable" class="table table-striped table-bordered" style="width:100%;border-collapse: collapse !important;">
                    <thead>
                        <tr>
                            <th style="width:141px;">Website Name</th>
                            <th style="width:134px;">Logo Image</th>
                            <th style="width:306px;">Logo Text</th>
                            <th style="width:306px;">Language</th>
                            <th style="width:306px;">Action</th>
                        </tr>
                    </thead> 
                    <tbody>
                        @foreach($setting as $s)
                            <tr>
                                <td>{{ $s->website_name}}</td>
                                <td>{{ $s->logo_image }}</td>
                                <td>{{ $s->logo_text }}</td>
                                <td>{{ $s->language }}</td>
                                <td>
                                    <a class="btn-primary btn1" href="{{url('setting/edit/'.$s->id)}}"><i class="fa fa-edit "></i>Edit</a>
                                    <a class="btn-danger btn1" href="{{url('setting/delete/'.$s->id)}}"><i class="fa fa-ban"></i></i>Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>   
@endsection