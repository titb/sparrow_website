@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
            @include('errors.message_error')
                <h2>Edit Setting</h2>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <form action="{{url('setting/update/'.$setting->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="mb-3">
                                <label>Website Name</label>
                                <input type="text" name="website_name" class="form-control" value="{{ $setting->website_name }}">
                            </div>
                           <div class="mb-3"> 
                                <label>Website URL</label>
                                <input type="text" name="website_url" class="form-control" value="{{ $setting->website_url }}">
                            </div>
                            <div class="mb-3">
                                <label>Language</label>
                                <select class="form-control" name="language">
                                    <option value="eng"<?php if($setting->language=='eng' ) echo"selected"?>>English</option>
                                    <option value="kh"<?php if($setting->language=='kh' ) echo"selected"?>>Khmer</option>
                                <select>
                            </div>
                            <div class="mb-3">
                                <label>Phone</label>
                                <input type="text" name="phone" class="form-control" value="{{ $setting->phone }}">
                            </div>
                            <div class="mb-3">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" value="{{ $setting->email }}">
                            </div>
                            <div class="mb-3">
                                <label>If Slide Show Only Home Page &nbsp;&nbsp;&nbsp;</label>
                                <input type="checkbox" name="is_slide_only_page" value="{{ $setting->is_slide_only_page }}" <?php if($setting->is_slide_only_page==1) echo"checked"?>
                            </div>
                            <div class="mb-3">
                                <label>Logo image</label>
                                <input type="file" name="logo_image" class="form-control" id="file">
                                @if($setting->logo_image)
                                    <img src="{{ url('images/'.$setting->logo_image) }}" class="close_img" style="width:100px;"/>
                                    <a class="btn btn-danger closes"><i class="fa fa-ban"></i></a>
                                    <input type="hidden" name="image_hidden" class="image-hidden" class="form-control" value="{{ $setting->logo_image }}">
                                @endif
                            </div>
                            <div class="mb-3">
                                <label >Text Logo</label>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="card card-outline card-info">
                                        <!-- /.card-header -->
                                        <div class="card-body pad">
                                            <div class="mb-3">
                                                <textarea name="text_logo" class="textarea" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                    {{ $setting->logo_text}}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>Favicon Images</label>
                                <input type="file" name="favicon_image" class="form-control" id="file">
                                @if($setting->favicon_image)
                                    <img src="{{ url('images/'.$setting->favicon_image) }}" class="fav-closes" style="width:50px; height: 50px;"/>
                                    <a class="btn btn-danger fav-click"><i class="fa fa-ban"></i></a>
                                    <input type="hidden" name="fav_image_hidden" class="fav-image-hidden" class="form-control" value="{{ $setting->favicon_image }}">
                                @endif
                            </div>
                            <div class="mb-3">
                                <label >Work Time</label>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="card card-outline card-info">
                                        <!-- /.card-header -->
                                        <div class="card-body pad">
                                            <div class="mb-3">
                                                <textarea name="work_time" class="textarea" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                {{ $setting->work_time}}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>Link Facebook</label>
                                <input type="text" name="link_fb" class="form-control" value="{{ $setting->link_fb}}">
                            </div>
                            <div class="mb-3">
                                <label >Copy Right</label>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="card card-outline card-info">
                                        <!-- /.card-header -->
                                        <div class="card-body pad">
                                            <div class="mb-3">
                                                <textarea name="copyright" class="textarea" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $setting->copyright }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label >Website Address</label>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="card card-outline card-info">
                                        <!-- /.card-header -->
                                        <div class="card-body pad">
                                            <div class="mb-3">
                                                <textarea name="address_site" class="textarea" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $setting->address_site }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label >Address</label>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="card card-outline card-info">
                                        <!-- /.card-header -->
                                        <div class="card-body pad">
                                            <div class="mb-3">
                                                <textarea name="address" class="textarea" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                    {{ $setting->address }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    $(".closes").click(function(){
    $(this).remove();
    $(".close_img").remove();
    $(".image-hidden").removeAttr("value");
    });
    
    $(".fav-click").click(function(){
    $(this).remove();
    $(".fav-closes").remove();
    $(".fav-image-hidden").removeAttr("value");
    });
</script>
<!-- <script>
  $(document).ready(function () {
    $("#input-b6").fileinput({
      theme: 'fa',
      uploadUrl: "form",
      showUpload: false,
      showCancel: true,
      initialPreviewAsData: true,
      overwriteInitial: false,
      allowedFileExtensions: ['jpg', 'png', 'gif'],
      maxFileSize: 2000,
      maxFileNum: 8,
      dataType: 'json',
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc|docx|pdf|txt)$/i,

    });
  });
</script> -->
@endsection