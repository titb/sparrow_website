@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
            @include('errors.message_error')
                <h2>Create Category</h2>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <form action="{{url('category/create_category')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="mb-3">
                                <label>Title</label>
                                <input type="text" name="name" class="form-control">
                            </div>

                            <div class="mb-3">
                            <label >Description</label>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="card card-outline card-info">
                                        <!-- /.card-header -->
                                        <div class="card-body pad">
                                            <div class="mb-3">
                                                <textarea name="description" class="textarea" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>Styles:</label>
                                <select name="block" class="form-control">
                                <option value="non">None</option>
                                <option value="about_us">About Us</option>
                                <option value="about_cjbi">About Cjbi</option>
                                <option value="event">Event  </option>
                                <option value="upcoming_event">Upcoming event  </option>
                                <option value="partner">Partner  </option>
                                                    <option value="information">Information  </option>
                                <option value="team">Team</option>
                                <option value="slide">Slide</option>
                                                <option value="welcom">WELCOME TO CJBI</option>
                                <option value="contact_us"> Contact Us</option>
                                <option value="become_member">Become Member</option>
                                <option value="single">Single</option>
                                <option value="form_profile">Form Profile</option>
                                <option value="form_member">Form Member</option>
                                <option value="member_login">Login Form</option>
                                <option value="gallery">Gallery</option>
                                
                                </select>
                            </div>
                            <div class="mb-3">
                                <label>Parent</label>
                                <select name="parent_id" class="form-control">
                                <?php $cate = App\Models\Category::where('category_type','=','category')->get();?>
                                    <option value="">Select Parent Category</option>
                                @foreach($cate as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Not Active</option>
                                </select>
                            </div>

                            <div class="mb-3">
                            <label>Publish Date</label>
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' name="publish_date" class="form-control" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>Unpublish Date</label>
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' name="unpublish_date" class="form-control" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
<style>
  .switch {
    position: relative;
    display: block;
    vertical-align: top;
    width: 100px;
    height: 30px;
    padding: 3px;
    margin: 0 10px 10px 0;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
    border-radius: 18px;
    box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
    cursor: pointer;
    box-sizing: content-box;
  }

  .switch-input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    box-sizing: content-box;
  }

  .switch-label {
    position: relative;
    display: block;
    height: inherit;
    font-size: 10px;
    text-transform: uppercase;
    background: #eceeef;
    border-radius: inherit;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
    box-sizing: content-box;
  }

  .switch-label:before,
  .switch-label:after {
    position: absolute;
    top: 50%;
    margin-top: -.5em;
    line-height: 1;
    -webkit-transition: inherit;
    -moz-transition: inherit;
    -o-transition: inherit;
    transition: inherit;
    box-sizing: content-box;
  }

  .switch-label:before {
    content: attr(data-off);
    right: 11px;
    color: #aaaaaa;
    text-shadow: 0 1px rgba(255, 255, 255, 0.5);
  }

  .switch-label:after {
    content: attr(data-on);
    left: 11px;
    color: #FFFFFF;
    text-shadow: 0 1px rgba(0, 0, 0, 0.2);
    opacity: 0;
  }

  .switch-input:checked~.switch-label {
    background: #02C4FD;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
  }

  .switch-input:checked~.switch-label:before {
    opacity: 0;
  }

  .switch-input:checked~.switch-label:after {
    opacity: 1;
  }

  .switch-handle {
    position: absolute;
    top: 4px;
    left: 4px;
    width: 28px;
    height: 28px;
    background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
    background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
    border-radius: 100%;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
  }

  .switch-handle:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -6px 0 0 -6px;
    width: 12px;
    height: 12px;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
    border-radius: 6px;
    box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
  }

  .switch-input:checked~.switch-handle {
    left: 74px;
    box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
  }

  /* Transition
   ========================== */
  .switch-label,
  .switch-handle {
    transition: All 0.3s ease;
    -webkit-transition: All 0.3s ease;
    -moz-transition: All 0.3s ease;
    -o-transition: All 0.3s ease;
  }
</style>