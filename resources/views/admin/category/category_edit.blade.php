@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
                <h2>Edit Category</h2>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <form action="{{url('category/update/'.$cat->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="modal-body">
                        <div class="mb-3">
                            <label>Title</label>
                            <input type="text" name="name" class="form-control" value="{{ $cat->name }}">
                        </div>
						
                        <div class="mb-3">
                            <label >Description</label>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="card card-outline card-info">
                                    <!-- /.card-header -->
                                    <div class="card-body pad">
                                        <div class="mb-3">
                                            <textarea name="description" class="textarea" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label>Style</label>
                            <select name="block" class="form-control">
                                <option value="non">None</option>
                                <option value="about_us" <?php if($cat->block == "about_us"){ echo "selected";}  ?> >About Us</option>
                                <option value="about_cjbi" <?php if($cat->block == "about_cjbi"){ echo "selected";}  ?> >About Cjbi</option>
                                <option value="upcoming_event" <?php if($cat->block == "upcoming_event"){ echo "selected";}  ?> >Upcoming Event  </option>
                                <option value="event" <?php if($cat->block == "event"){ echo "selected";}  ?> >Event  </option>
                                <option value="partner" <?php if($cat->block == "partner"){ echo "selected";}  ?> >Partner</option>
                                <option value="team" <?php if($cat->block == "team"){ echo "selected";}  ?> >Team</option>
                                <option value="contact_us" <?php if($cat->block == "contact_us"){ echo "selected";}  ?> >Contact Us</option>
                                <option value="become_member" <?php if($cat->block == "become_member"){ echo "selected";}  ?> > Become Member</option>
                                <option value="slide" <?php if($cat->block == "slide"){ echo "selected";}  ?> > Slide</option>
                                <option value="welcom" <?php if($cat->block == "welcom"){ echo "selected";}  ?> > Welcome</option>
                                <option value="single" <?php if($cat->block == "single"){ echo "selected";}  ?> > Single</option>
                                <option value="information" <?php if($cat->block == "information"){ echo "selected";}  ?> > Information</option>
                                <option value="form_profile" <?php if($cat->block == "form_profile"){ echo "selected";}  ?> > Form Profile</option>
                                <option value="form_member" <?php if($cat->block == "form_member"){ echo "selected";}  ?> > Form Member</option>
                                <option value="member_login" <?php if($cat->block == "member_login"){ echo "selected";}  ?> > Login Form</option>
                                <option value="gallery" <?php if($cat->block == "gallery"){ echo "selected";}  ?> > Gallery</option>
                            </select>
                        </div>
                        <div class="mb-3">
                        <label>Parent</label>
                        <select class="form-control" name="parent_id">
                            <<option value="">Select Parent Category</option>
                            @foreach($cate as $c)
                                <option value="{{$c->id}}" <?php if($c->id == $cat->parent_id){echo "selected";}?> >{{$c->name}}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="mb-3">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option value="1" <?php if($cat->status == 1){echo "selected";} ?> >Active</option>
                                <option value="0" <?php if($cat->status == 0){echo "selected";} ?> >Not Active</option>
                            </select>
                        </div>

                        <div class="mb-3">
                                <label>Publish Date</label>
                                <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' name="publish_date" class="form-control" value="{{ date('d-m-Y',strtotime($cat->publish_date)) }}"/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>Unpublish Date</label>
                                <div class='input-group date' id='datetimepicker2'>
                                        <input type='text' name="unpublish_date" class="form-control" value="{{ date('d-m-Y',strtotime($cat->unpublish_date)) }}"/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection