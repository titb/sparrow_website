@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        <div class="title_left">
            <h3>Category</h3>
            @include('errors.message_error')
        </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
            <a class="create" href="{{('create_category')}}">Create Category</a>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <a href="{{url('multiple_delete_menu')}}" class="all_delete Delete" style="background:;color:red;margin-bottom:5px;">Delete All</a>
                        <table id="datatable" class="table table-striped table-bordered" style="width:100%; border-collapse: collapse !important;">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Style</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($category)>0)
                            @foreach($category as $key => $ca)
                                <tr>

                                    <td>{{ $ca->name }}</td>
                                    <td>{{ $ca->block }}</td>
                                    <td>
                                        @if($ca->status == 1)
                                            <span>Active</span>
                                        @else
                                            Not Active
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('category/edit/'.$ca->id)}}" class="btn-primary btn1" ><i class="fa fa-edit "></i> Edit</a>
                                        <a href="{{ url('category/delete/'.$ca->id) }}" class="btn-danger btn1"><i class="fa fa-ban"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection