@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">

                <h2>Edit Post</h2>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <form action="{{url('post/update/'.$post->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                        <div class="mb-3">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" value="{{ $post->title }}">
                        </div>
                        <div class="mb-3">
                            <label>Template</label><br>
                            <select class="form-control" name="template">
                                <option value="">Select Template</option>
                                <option value="multi_post_title" <?php if($post->template=='multi_post_title')echo "selected"; ?>>Multiple Post Title</option>
                                <option value="multi_post" <?php if($post->template=='multi_post')echo "selected"; ?>>Multiple Post</option>
                                <option value="post_detail" <?php if($post->template=='post_detail')echo "selected"; ?>>Post Detail</option>
                                <option value="single_post" <?php if($post->template=='single_post')echo "selected"; ?>>Single Post</option>
                                <option value="slick_slider" <?php if($post->template=='slick_slider')echo "selected"; ?>>Slick Slider</option>
                                <option value="slide" <?php if($post->template=='slide')echo "selected"; ?>>Slide</option>
                                <option value="request" <?php if($post->template=='request')echo "selected"; ?>>Request</option>
                                <option value="subscription" <?php if($post->template=='subscription')echo "selected"; ?>>Subscription</option>
                                <option value="contact" <?php if($post->template=='contact')echo "selected"; ?>>Contact Us</option>
                                <option value="about_us" <?php if($post->template=='about_us')echo "selected"; ?>>About Us</option>
                                <option value="recent_news" <?php if($post->template=='recent_news')echo "selected"; ?>>News</option>

                            </select>
                        </div>
                        <div class="mb-3">
                            <label>Post Type</label>
                            <select class="form-control main" name="post_type">
                              <option value="single" <?php if($post->post_type=='single') echo "selected"; ?>>Single</option>
                              <option value="multi" <?php if($post->post_type=='multi') echo "selected"; ?> >Multiple</option>
                            <select>
                        </div>
                        <?php
                        $SUB_POST=[];
                        foreach($post->sub_posts as $sub)
                            array_push($SUB_POST,$sub->id);
                        ?>
                        <div class="mb-3 hide">
                            <label>Sub Post</label>
                            <select class="js-example-basic-multiple form-control sub" name="sub_post_id[]" multiple="multiple">
                                @foreach($posts as $p)
                                    <option value="{{ $p->id }}"<?php if(in_array($p->id, $SUB_POST)) echo"selected";?>>{{$p->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3" style="display: -webkit-inline-box;">
                            <div class="btn-check">
                                <label for="">show title</label>
                                <label class="switch">
                                <input class="switch-input show_title" type="checkbox" id="switch_check" name="show_title" value="{{$post->action_title}}" 
                                @if($post->action_title==1)
                                    checked
                                @else                            
                                @endif
                                >
                                <span class="switch-label" data-on="On" data-off="Off"></span>
                                <span class="switch-handle"></span>
                                </label>
                            </div>
                            <div class="btn-check">
                            <label for="">show Featured Image</label>
                            <label class="switch">
                                <input class="switch-input show_image_feature" type="checkbox" id="switch_show_image_feature"
                                name="show_image_feature" value="{{$post->show_image_feature}}" 
                                @if($post->show_image_feature==1)
                                    checked
                                @else                            
                                @endif
                                >
                                <span class="switch-label" data-on="On" data-off="Off"></span>
                                <span class="switch-handle"></span>
                            </label>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label>Link</label>
                            <input type="text" name="link" class="form-control" value="{{ $post->link }}">
                        </div>
                        <div class="mb-3">
                            <label>Post Format</label><br>
                            <select class="form-control" name="categorie_id[]">
                                <option value="0">Select Post Format</option>
                                <option value="standard" <?php  if($post->post_format == 'standard'){echo "selected";}  ?>>Standard</option>
                                <option value="file" <?php  if($post->post_format == 'file'){echo "selected";}  ?>>File</option>
                                <option value="image" <?php  if($post->post_format == 'image'){echo "selected";}  ?>>Image</option>
                                <option value="gallery" <?php  if($post->post_format == 'gallery'){echo "selected";}  ?>>Gallery</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label>Translate</label>
                            <select class="form-control" name="translate">
                                <option value="">Select Page</option>
                                @foreach($post_tr as $p)
                                    <option value="{{$p->link}}">{{$p->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label>Link Download</label>
                            <input type="text" name="link_download" class="form-control" value="{{ $post->link_download }}">
                        </div>
                        <div class="mb-3">
                            <label>Facebook Link</label>
                            <input type="text" name="location_job" class="form-control" value="{{$post->location_job}}">
                        </div>
                        <div class="mb-3">
                            <label >Description</label>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="card card-outline card-info">
                                    <!-- /.card-header -->
                                    <div class="card-body pad">
                                        <div class="mb-3">
                                            <textarea name="description" class="textarea" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$post->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option value="1" <?php if($post->status == 1){echo "selected";} ?> >Active</option>
                                <option value="0" <?php if($post->status == 0){echo "selected";} ?> >Not Active</option>
                            </select>
                        </div>
                        <div class="mb-3">
                        <label>Publish Date</label>
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' name="publish_date" class="form-control" value="{{ date('d-m-Y h:i:s',strtotime($post->publish_date)) }}"/>
                            <span class="input-group-addon"><span class="fa fa-calendar"></span>
                            </span>
                        </div>
                        </div>
                        <div class="mb-3">
                        <label>Unpublish Date</label>
                        <div class='input-group date' id='datetimepicker2'>
                            <input type='text' name="unpublish_date" class="form-control"  value="{{ date('d-m-Y',strtotime($post->unpublish_date)) }}"/>
                            <span class="input-group-addon"><span class="fa fa-calendar"></span>
                            </span>
                        </div>
                        </div>
                        <div class="mb-3">
                            <label>Choose image</label>
                            <input type="file" name="image" class="form-control">
                            @if($post->image)
                                <img src="{{ url('images/'.$post->image) }}" class="img-close" style="width:100px;"/>
                                <a class="btn btn-danger closes"><i class="fa fa-ban"></i></a>
                                <input type="hidden" name="image_hidden" class="image-hidden" class="form-control" value="{{ $post->image }}">
                            @endif
                        </div>
                        <div class="mb-3">
                            <label>Choose Image Multi Upload</label>
                            <input type="file" id="multi" name="image_multi[]" class="form-control" multiple>
                            @if($post->image_post)
                            @foreach ($post->image_post as $key => $value)
                                <img src="{{ url('Galleries/'.$value->image) }}" class="mult-img-close" style="width:100px;"/>
                            @endforeach
                            @if($post->image_post->sum('post_id') > 0)
                                <a class="btn btn-danger mult_closes"><i class="fa fa-ban"></i></a>
                                <input type="hidden" id="mult_image_hidden" name="mult_image_hidden" class="image-hidden" class="form-control" value="1">
                            @endif
                        @endif
                        </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
  $('.main').change(function(){
  if($(this).val() == 'multi'){
    $('.hide').css('display','block');
  }else{
    $('.hide').css('display','none');
  }
});
</script>
<script>
	    $(".closes").click(function(){
	        $(this).remove();
	        $(".image-hidden").removeAttr("value");
	        $(".img-close").remove();
	    });
	    
	    $(".mult_closes").click(function(){
	        $(this).remove();
	        $("#mult_image_hidden").attr("value","0");
	        $(".mult-img-close").remove();
	    });
	</script>

  <script>
    $("#switch_check").on('change', function(){
      if($(this).is(':checked')){
         $(this).attr('value','1');
         $(this).attr("checked", "checked");
      }else if($(this).val('" "')){
         $(this).attr('value', '0');
         $(this).removeAttr("checked", "checked");
      }
    });



    $("#feature_check").on('change', function(){
        if($(this).is(':checked')){
          $(this).attr('value','1');
          $(this).attr("checked", "checked");
        }else if($(this).val('" "')){
          $(this).attr('value', '0');
          $(this).removeAttr("checked", "checked");
        }
    });
</script>
@endsection
<style>
  .switch {
    position: relative;
    display: block;
    vertical-align: top;
    width: 100px;
    height: 30px;
    padding: 3px;
    margin: 0 10px 10px 0;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
    border-radius: 18px;
    box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
    cursor: pointer;
    box-sizing: content-box;
  }

  .switch-input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    box-sizing: content-box;
  }

  .switch-label {
    position: relative;
    display: block;
    height: inherit;
    font-size: 10px;
    text-transform: uppercase;
    background: #eceeef;
    border-radius: inherit;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
    box-sizing: content-box;
  }

  .switch-label:before,
  .switch-label:after {
    position: absolute;
    top: 50%;
    margin-top: -.5em;
    line-height: 1;
    -webkit-transition: inherit;
    -moz-transition: inherit;
    -o-transition: inherit;
    transition: inherit;
    box-sizing: content-box;
  }

  .switch-label:before {
    content: attr(data-off);
    right: 11px;
    color: #aaaaaa;
    text-shadow: 0 1px rgba(255, 255, 255, 0.5);
  }

  .switch-label:after {
    content: attr(data-on);
    left: 11px;
    color: #FFFFFF;
    text-shadow: 0 1px rgba(0, 0, 0, 0.2);
    opacity: 0;
  }

  .switch-input:checked~.switch-label {
    background: #02C4FD;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
  }

  .switch-input:checked~.switch-label:before {
    opacity: 0;
  }

  .switch-input:checked~.switch-label:after {
    opacity: 1;
  }

  .switch-handle {
    position: absolute;
    top: 4px;
    left: 4px;
    width: 28px;
    height: 28px;
    background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
    background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
    border-radius: 100%;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
  }

  .switch-handle:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -6px 0 0 -6px;
    width: 12px;
    height: 12px;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
    border-radius: 6px;
    box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
  }

  .switch-input:checked~.switch-handle {
    left: 74px;
    box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
  }

  /* Transition
   ========================== */
  .switch-label,
  .switch-handle {
    transition: All 0.3s ease;
    -webkit-transition: All 0.3s ease;
    -moz-transition: All 0.3s ease;
    -o-transition: All 0.3s ease;
  }
</style>
