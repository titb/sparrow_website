@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        <div class="title_left">
            <h3>Post</h3>
        </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
            @include('errors.message_error')
            <a class="create" href="{{('create_post')}}">Create Post</a>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <table id="datatable" class="table table-striped table-bordered" style="width:100%;border-collapse: collapse !important;">
                    <thead>
                        <tr>
                            <th style="width:141px;">Title</th>
                            <th style="width:134px;">Status</th>
                            <th style="width:306px;">Action</th>
                        </tr>
                    </thead> 
                    <tbody>
                        @foreach($post as $p)
                            <tr>
                                <td>{{ $p->title }}</td>
                                <td>
                                  @if($p->status == 0)
                                    Not Active
                                  @else
                                    <span>Active</span>
                                  @endif
                                </td>
                                <td>
                                    <a class="btn-primary btn1" href="{{url('post/edit/'.$p->id)}}"><i class="fa fa-edit "></i>Edit</a>
                                    <a class="btn-danger btn1" href="{{url('post/delete/'.$p->id)}}"><i class="fa fa-ban"></i></i>Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
