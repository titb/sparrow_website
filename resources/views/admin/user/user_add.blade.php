@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
            @include('errors.message_error')
                <h2>Create User</h2>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <form action="{{url('user/create_user')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                        <div class="mb-3">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Name">
                        </div>
                        <div class="mb-3">
                            <label>User Name</label>
                            <input type="text" name="username" class="form-control" placeholder="Username">  
                        </div>
                        <div class="mb-3">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" placeholder="Email">  
                        </div>
                        <div class="mb-3">
                            <label>Phone</label>
                            <input type="text" name="phone" class="form-control" placeholder="Phone">  
                        </div>

                        <div class="mb-3">
                            <label>Branch</label>
                            <select name="branch" id="" class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label>Stutus</label>
                            <select class="form-control" name="status">
                                <option value="1">Active</option>
                                <option value="0">Not Active</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password"/>
                        </div>
                        <div class="mb-3">
                            <label>Comfirm Password</label>
                            <input type="password" class="form-control" name="comfirm_password"/>
                        </div>
                        <div class="mb-3">
                            <label>Photo</label>
                            <input type="file" name="image" class="form-control">
                        </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<!-- <script>
  $(document).ready(function () {
    $("#input-b6").fileinput({
      theme: 'fa',
      uploadUrl: "form",
      showUpload: false,
      showCancel: true,
      initialPreviewAsData: true,
      overwriteInitial: false,
      allowedFileExtensions: ['jpg', 'png', 'gif'],
      maxFileSize: 2000,
      maxFileNum: 8,
      dataType: 'json',
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc|docx|pdf|txt)$/i,

    });
  });
</script> -->
@endsection
