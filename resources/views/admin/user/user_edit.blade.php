@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
            @include('errors.message_error')
                <h2>Edit Setting</h2>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <form action="{{url('user/update/'.$user->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="mb-3">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                            </div>
                           <div class="mb-3"> 
                                <label>User Name</label>
                                <input type="text" name="username" class="form-control" value="{{ $user->username }}">
                            </div>
                            <div class="mb-3"> 
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" value="{{ $user->email }}">
                            </div>
                            <div class="mb-3"> 
                                <label>Phone</label>
                                <input type="text" name="phone" class="form-control" value="{{ $user->phone }}">
                            </div>
                            <div class="mb-3">
                                <label>Branch</label>
                                <select class="form-control" name="branch">
                                    <option value="{{ $user->branch }}"></option>
                                <select>
                            </div>
                            <div class="mb-3">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="1" <?php if($user->status == 1){echo "selected";} ?> >Active</option>
                                    <option value="0" <?php if($user->status == 0){echo "selected";} ?> >Not Active</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label>Photo</label>
                                <input type="file" class="form-control" name="image" />
                                @if($user->image)
                                    <img src="{{url('images/'.$user->image)}}" class="img-close" style="width: 150px;" />
                                    <a class="btn btn-danger closes"><i class="fa fa-ban"></i></a>
                                    <input type="hidden" name="image_hidden" class="image-hidden" class="form-control" value="{{ $user->image }}">
                                @endif
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    $(".closes").click(function(){
        $(this).remove();
        $(".image-hidden").removeAttr("value");
        $(".img-close").remove();
    });
</script>
<!-- <script>
  $(document).ready(function () {
    $("#input-b6").fileinput({
      theme: 'fa',
      uploadUrl: "form",
      showUpload: false,
      showCancel: true,
      initialPreviewAsData: true,
      overwriteInitial: false,
      allowedFileExtensions: ['jpg', 'png', 'gif'],
      maxFileSize: 2000,
      maxFileNum: 8,
      dataType: 'json',
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc|docx|pdf|txt)$/i,

    });
  });
</script> -->
@endsection