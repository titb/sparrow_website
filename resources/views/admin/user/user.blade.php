@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        <div class="title_left">
            <h3>User Management</h3>
        </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
            @include('errors.message_error')
            <a class="create" href="{{('create_user')}}">Create User</a>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                    <table id="datatable" class="table table-striped table-bordered" style="width:100%;border-collapse: collapse !important;">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Status</th>
                            <!-- <th>Permission</th> -->
                            <!-- <th>Branch</th> -->
                            <th>Action</th>
                        </tr>
                    </thead> 
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name}}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone}}</td>
                                <td>
	                                	@if($user->status == 1)
	                                		<span>Active</span>
	                                	@else
	                                		Not Active
	                                	@endif
	                                </td>
                                <td>
                                    <a class="btn-primary btn1" href="{{url('user/change_pw/'.$user->id)}}"><i class="fa fa-edit "></i>Change Password</a>
                                    <a class="btn-primary btn1" href="{{url('user/edit/'.$user->id)}}"><i class="fa fa-edit "></i>Edit</a>
                                    <a class="btn-danger btn1" href="{{url('user/delete/'.$user->id)}}"><i class="fa fa-ban"></i></i>Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>   
@endsection