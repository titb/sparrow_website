@extends('admin.master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="">
            <div class="x_title">
            @include('errors.message_error')
                <h2>Create Menu</h2>
                
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                <div class="col-md-12">
                    <div class="card-box table-responsive">
                    <form action="{{url('admin/create_menu')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="modal-body">
                        <div class="col-md-6">
                            <div class="x_panel">
                                <div class="mb-3">
                                    <label>Title</label>
                                    <input type="text" name="name" class="form-control" placeholder="Title">
                                </div>
                                <div class="mb-3">
                                    <label>Parent</label>
                                    <select class="form-control" name="parent">
                                    <option value="0" style="">Select Parent</option>
                                    @foreach($menu as $m)
                                        <option value="{{$m->id}}">{{$m->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="mb-3" style="display: -webkit-inline-box;">
                                  <div class="btn-check">
                                      <label for="">show banner</label>
                                      <label class="switch">
                                      <input class="switch-input show_title" type="checkbox" id="switch_check" name="show_banner" value="1" checked>
                                      <span class="switch-label" data-on="On" data-off="Off"></span>
                                      <span class="switch-handle"></span>
                                      </label>
                                  </div>
                                </div>
                                <div class="mb-3">
                                    <label>Link</label>
                                    <input type="text" name="link" class="form-control" placeholder="link">
                                </div>
                                <div class="mb-3">
                                    <label>Menu Type</label>
                                    <select class="form-control" name="menu_type">
                                    <option value="0">Select Menu Type</option>
                                        @if(count($menu_type)>= 0)
                                        @foreach($menu_type as $men)
                                            <option value="{{ $men->id }}">{{ $men->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label>Language</label>
                                    <select class="form-control" name="language">
                                    <option value="0">Select Language</option>
                                        <option value="">Home</option>
                                        <option value="">Home</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label>Translate To</label>
                                    <select class="form-control" name="translation">
                                    <option value="0">Select Translate</option>
                                    @foreach($menu as $m)
                                    <option value="{{ $m->link }}">{{ $m->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label>Order Menu</label>
                                    <select class="form-control" name="ordering">
                                    <option value="0">Select Order Menu</option>
                                        <option value="">Home</option>
                                        <option value="">Home</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label>Status</label>
                                    <!-- <input type="text" name="parent" class="form-control" placeholder="Parent"> -->
                                    <select class="form-control" name="status">
                                        <option value="1">Active</option>
                                        <option value="0">Not Active</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label>Choose image</label>
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="x_panel">
                                <div class="mb-3">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                                <tr>
                                                    <th>Select Post</th>
                                                    <th>Select Postion</th>
                                                    <th><a href="#" id="add_on_rows" class="btn1 btn-success">Add</a></th>
                                                </tr>
                                        </thead>
                                        <tbody id="body_add">
                                    
                                            <tr id="add_row">
                                                <!-- <td>
                                                    <select class="form-control" name="post_id[]">
                                                        @if(count($post)>0)
                                                            <option value="">Select Post</option>
                                                                @foreach($post as $key => $p)
                                                                        <option value="{{ $p->id }}">{{$p->title}}</option>
                                                                @endforeach
                                                            @endif
                                                    </select>
                                                </td>

                                                <td>
                                                    <select class="form-control" name="position[]">
                                                            <option value="position_1">Position 1</option>
                                                            <option value="position_2">Position 2</option>
                                                            <option value="position_3">Position 3</option>
                                                            <option value="position_4">Position 4</option>
                                                            <option value="position_5">Position 5</option>
                                                            <option value="position_6">Position 6</option>
                                                            <option value="position_7">Position 7</option>
                                                            <option value="position_8">Position 8</option>
                                                    </select>
                                                </td>
                                                <td><a href="#" class="btn1 btn-danger remove_rows" >delete</a></td>
                                            </tr> -->
                                        </body>
                                    </table>
                                </div>  
                            </div>
					    </div>
                        </div> 
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$('#add_on_rows').click(function(event){
				 event.preventDefault();
						var rows = '<tr id="add_row">' +
											 '<td>' +
											 '<select class="form-control" name="post_id[]">' +
                                                '@if(count($post)>0)' +
                                                '<option value="">Select Post</option>' +
                                                    '@foreach($post as $key => $p)' +
                                                        '<option value="{{ $p->id }}">{{ $p->template }}</option>' +
                                                    '@endforeach' +
                                                '@endif' +
                                            '</select>'  +
	 									'</td>'  +
											 '<td>'+
											 	'<select class="form-control" name="position[]">' +
	 											'<option value="position_1">Position 1</option>' +
	 											'<option value="position_2">Position 2</option>' +
	 											'<option value="position_3">Position 3</option>' +
	 											'<option value="position_4">Position 4</option>' +
	 											'<option value="position_5">Position 5</option>' +
	 											'<option value="position_6">Position 6</option>' +
	 											'<option value="position_7">Position 7</option>' +
	 											'<option value="position_8">Position 8</option>' +
	 										'</select>' +
											 '</td>' +
											 '<td><a href="#" class="btn1 btn-danger remove_rows" >delete</a></td>'
											 '</tr>';
					 $('#body_add').append(rows)
			 });
			 $('#body_add').on('click','.remove_rows',function(event){
						 event.preventDefault();
						$(this).parent().parent().remove();
				});
});
</script>
<script>
  $(document).ready(function () {
    // add/remove checked class

    $('body').on('click', '#DeleteImg', function () {
      alert('44');
      var title = $(this).attr('value');
      var label = $("#label").attr('value');
      var labels = $('#labels');
      if (title == label) {
        labels.remove();
      }

    });

    $('body').on('click', 'a', function () {
      var text = "";
      var array = [];
      var name_img = $('.name_images');
      $("input:checkbox[name=image_check]:checked").each(function (index, element) {
        array.push($(this).val());
        $('#GFG_DOWN').append($("<img src='images/" + array + "'>"));
        name_img.append(array);
      });
      text = text.substring(0, text.length - 1);
      var count = $("[type='checkbox']:checked").length;
      $('#count').val($("[type='checkbox']:checked").length);
    });

    $(".image-checkbox").each(function () {
      if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
        $(this).addClass('image-checkbox-checked');
      } else {
        $(this).removeClass('image-checkbox-checked');
      }
    });

    // sync the input state
    $(".image-checkbox").on("click", function (e) {
      $(this).toggleClass('image-checkbox-checked');
      var $checkbox = $(this).find('input[type="checkbox"]');
      $checkbox.prop("checked", !$checkbox.prop("checked"));

      e.preventDefault();
    });
  });

  $("#switch_check").on('change', function () {
    if ($(this).is(':checked')) {
      $(this).attr('value', '1');
      $(this).attr("checked", "checked");
    } else if ($(this).val('" "')) {
      $(this).attr('value', '0');
      $(this).removeAttr("checked", "checked");
    }
  });
  $("#switch_show_image_feature").on('change', function () {
    if ($(this).is(':checked')) {
      $(this).attr('value', '1');
      $(this).attr("checked", "checked");
    } else if ($(this).val('" "')) {
      $(this).attr('value', '0');
      $(this).removeAttr("checked", "checked");
    }
  });

  $("#feature_check").on('change', function () {
    if ($(this).is(':checked')) {
      $(this).attr('value', '1');
      $(this).attr("checked", "checked");
    } else if ($(this).val('" "')) {
      $(this).attr('value', '0');
      $(this).removeAttr("checked", "checked");
    }
  });
</script>
@endsection
<style>
  .switch {
    position: relative;
    display: block;
    vertical-align: top;
    width: 100px;
    height: 30px;
    padding: 3px;
    margin: 0 10px 10px 0;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
    border-radius: 18px;
    box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
    cursor: pointer;
    box-sizing: content-box;
  }

  .switch-input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    box-sizing: content-box;
  }

  .switch-label {
    position: relative;
    display: block;
    height: inherit;
    font-size: 10px;
    text-transform: uppercase;
    background: #eceeef;
    border-radius: inherit;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
    box-sizing: content-box;
  }

  .switch-label:before,
  .switch-label:after {
    position: absolute;
    top: 50%;
    margin-top: -.5em;
    line-height: 1;
    -webkit-transition: inherit;
    -moz-transition: inherit;
    -o-transition: inherit;
    transition: inherit;
    box-sizing: content-box;
  }

  .switch-label:before {
    content: attr(data-off);
    right: 11px;
    color: #aaaaaa;
    text-shadow: 0 1px rgba(255, 255, 255, 0.5);
  }

  .switch-label:after {
    content: attr(data-on);
    left: 11px;
    color: #FFFFFF;
    text-shadow: 0 1px rgba(0, 0, 0, 0.2);
    opacity: 0;
  }

  .switch-input:checked~.switch-label {
    background: #02C4FD;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
  }

  .switch-input:checked~.switch-label:before {
    opacity: 0;
  }

  .switch-input:checked~.switch-label:after {
    opacity: 1;
  }

  .switch-handle {
    position: absolute;
    top: 4px;
    left: 4px;
    width: 28px;
    height: 28px;
    background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
    background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
    border-radius: 100%;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
  }

  .switch-handle:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -6px 0 0 -6px;
    width: 12px;
    height: 12px;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
    border-radius: 6px;
    box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
  }

  .switch-input:checked~.switch-handle {
    left: 74px;
    box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
  }

  /* Transition
   ========================== */
  .switch-label,
  .switch-handle {
    transition: All 0.3s ease;
    -webkit-transition: All 0.3s ease;
    -moz-transition: All 0.3s ease;
    -o-transition: All 0.3s ease;
  }
</style>