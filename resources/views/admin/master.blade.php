<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>DataTables | Gentelella</title>

  <!-- Bootstrap -->
  <link href="cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link href="{{url('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{url('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{url('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
  <!-- iCheck -->
  <link href="{{url('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
  <!-- Datatables -->
  <!-- summernote -->
  <link rel="stylesheet" href="{{url('plugins/summernote/summernote-bs4.css')}}">
  <!-- data table -->
  <link href="{{url('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
  <!-- font style -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
  <!-- Custom Theme Style -->
  <link href="{{url('build/css/custom.min.css')}}" rel="stylesheet">
  <!-- css -->
  <link rel="stylesheet" href="{{url('admin_css/style.css')}}">
  <!-- datetime css -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
  <link href="{{ url('assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
  <!-- Select 2 -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="{{url('dist/js/select2.min.js')}}" defer></script>
  <link rel="stylesheet" href="{{url('dist/css/select2.min.css')}}">
  
  
  
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Sparrow Admin</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="{{url('assets/img/admin/find_user.png')}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome</span>
              <h2>Admin</h2>
            </div>
          </div>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>General Menu</h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-bars fa-3x"></i> Menu <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{url('admin')}}">Menu</a></li>
                    <li><a href="{{url('menu_type')}}">Menu Type</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-sitemap fa-3x"></i> Post <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{url('category')}}">Category</a></li>
                    <li><a href="{{url('post')}}">Post</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-users fa-3x"></i> User <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{url('user')}}">User</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-cog fa-3x"></i> Setting <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{url('setting')}}">Setting</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
          </div>
          <nav class="nav navbar-nav">
            <ul class=" navbar-right">
              <div style="color: black;float: right;font-size: 16px;"><a href="{{ url('home') }}" target="_blank" style="color:black;">View Site</a> &nbsp; <?php  echo date('l')."&nbsp; ". date('d-M-Y'); ?> &nbsp;
              <li class="nav-item dropdown open" style="padding-left: 15px;">
                <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown"
                  data-toggle="dropdown" aria-expanded="false">
                  <img src="{{url('assets/img/logo/logo.jpg')}}" alt="">Sparrow
                </a>
                <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="javascript:;"> Profile</a>
                  <a class="dropdown-item" href="javascript:;">
                    <span class="badge bg-red pull-right">50%</span>
                    <span>Settings</span>
                  </a>
                  <a class="dropdown-item" href="javascript:;">Help</a>
                  <a class="dropdown-item" href="{{ url('logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                </div>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <!-- /top navigation -->
      <!-- page content -->
      @yield('content')
    </div>
  </div>
  <!-- /page content -->

  <!-- footer content -->

  <!-- /footer content -->
  <!-- jQuery -->
  <script src="{{url('vendors/jquery/dist/jquery.min.js')}}"></script>
  <!-- Bootstrap -->
  <script src="{{url('vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <!-- FastClick -->
  <script src="{{url('vendors/fastclick/lib/fastclick.js')}}"></script>
  <!-- NProgress -->
  <script src="{{url('vendors/nprogress/nprogress.js')}}"></script>
  <!-- iCheck -->
  <script src="{{url('vendors/iCheck/icheck.min.js')}}"></script>
  <!-- Datatables -->
  <script src="{{url('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{url('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
  <script src="{{url('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
  <script src="{{url('vendors/jszip/dist/jszip.min.js')}}"></script>
  <script src="{{url('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
  <script src="{{url('vendors/pdfmake/build/vfs_fonts.js')}}"></script>

  <!-- Custom Theme Scripts -->
  <script src="{{url('build/js/custom.min.js')}}"></script>
  <!-- Summernote -->
<script src="{{url('plugins/summernote/summernote-bs4.min.js')}}"></script>

<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
<!-- <script src="{{url('assets/js/bootstrap.min.js')}}"></script> -->
<script  src="{{ url('assets/js/moment.js') }}"></script>
<script  src="{{ url('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript">
       $(function () {
            var bindDatePicker = function() {
         		$(".date").datetimepicker({
                 format:'DD-MM-YYYY HH:mm:ss ',
         			icons: {
         				time: "fa fa-clock-o",
         				date: "fa fa-calendar",
         				up: "fa fa-arrow-up",
         				down: "fa fa-arrow-down"
         			}
         		}).find('input:first').on("blur",function () {
         			// check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
         			// update the format if it's yyyy-mm-dd
         			var date = parseDate($(this).val());

         			if (! isValidDate(date)) {
         				//create date based on momentjs (we have that)
         				date = moment().format('DD-MM-YYYY HH:mm ');
         			}

         			$(this).val(date);

         		});
         	}

            var isValidDate = function(value, format) {
         		format = format || false;
         		// lets parse the date to the best of our knowledge
         		if (format) {
         			value = parseDate(value);
         		}

         		var timestamp = Date.parse(value);

         		return isNaN(timestamp) == false;
            }

            var parseDate = function(value) {
         		var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
         		if (m)
         			value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

         		return value;
            }

            bindDatePicker();
          });
  </script>
  <script>
    $(document).ready(function() {
      $('.js-example-basic-multiple').select2();
    });
  </script>
  <!-- <script>
  $('.main').change(function() {
    var options = '';
    if($(this).val() == 'single') {
        options = '';
    }
    else if ($(this).val() == 'multi'){
        options = '<option value="1">Alabama</option>,<option value="1">Wyoming</option>';
    }

    $('.sub').html(options);
});
</script> -->

</body>
</html>