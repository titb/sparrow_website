@extends('layout.master')
@section('content')
    @if($menu->menu_posts)
        @foreach($menu->menu_posts as $p)
            @if($p->pivot->position == 'position_1')
                    @if($p->template)
                        <?php  $tem = $p->template; ?>
                    @else
                        <?php   $tem = "non"; ?>
                    @endif
                    @include('template.'.$tem)
            @endif
        @endforeach
        @foreach($menu->menu_posts as $p)
        @if($p->pivot->position == 'position_2')
                @if($p->template)
                    <?php  $tem = $p->template; ?>
                @else
                    <?php   $tem = "non"; ?>
                @endif
                @include('template.'.$tem)
        @endif

        @endforeach
        @foreach($menu->menu_posts as $p)
        @if($p->pivot->position == 'position_3')
                @if($p->template)
                    <?php  $tem = $p->template; ?>
                @else
                    <?php   $tem = "non"; ?>
                @endif
                @include('template.'.$tem)
        @endif

        @endforeach @foreach($menu->menu_posts as $p)
        @if($p->pivot->position == 'position_4')
                @if($p->template)
                    <?php  $tem = $p->template; ?>
                @else
                    <?php   $tem = "non"; ?>
                @endif
                @include('template.'.$tem)
        @endif
        @endforeach   
        @foreach($menu->menu_posts as $p)
            @if($p->pivot->position == 'position_5')
                    @if($p->template)
                        <?php  $tem = $p->template; ?>
                    @else
                        <?php   $tem = "non"; ?>
                    @endif
                    @include('template.'.$tem)
            @endif

        @endforeach
        @foreach($menu->menu_posts as $p)
            @if($p->pivot->position == 'position_6')
                    @if($p->template)
                        <?php  $tem = $p->template; ?>
                    @else
                        <?php   $tem = "non"; ?>
                    @endif
                    @include('template.'.$tem)
        @endif
        @endforeach
        @foreach($menu->menu_posts as $p)
            @if($p->pivot->position == 'position_7')
                    @if($p->template)
                        <?php  $tem = $p->template; ?>
                    @else
                        <?php   $tem = "non"; ?>
                    @endif
                    @include('template.'.$tem)
            @endif
        @endforeach
        @foreach($menu->menu_posts as $p)
            @if($p->pivot->position == 'position_8')
                    @if($p->template)
                        <?php  $tem = $p->template; ?>
                    @else
                        <?php   $tem = "non"; ?>
                    @endif
                    @include('template.'.$tem)
            @endif
        @endforeach

    @endif
@endsection
