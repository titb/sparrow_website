<div class="slider-area ">
    <!-- Mobile Menu -->
    <div class="single-slider slider-height2 d-flex align-items-center" data-background="{{url('assets/img/hero/contact_hero.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center">
                        <h2>{{$menu->name}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="breadcrumbs">
<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ol class="breadcrumb">
        <i class="fa fa-sitemap"></i><li><a href="{{url('home')}}">Home</a></li><span>/</span><li><a href="">{{$menu->name}}</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>