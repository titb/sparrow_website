<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;
use App\Http\Controllers\homecontroller;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\MenuTypeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ControllerSetting;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    // Admin Menu
    Route::get('/admin',[AdminController::class,'index']);
    Route::get('create_menu',[AdminController::class,'show']);
    Route::post('admin/create_menu',[AdminController::class,'insert']);
    Route::get('admin/edit/{id}',[AdminController::class,'get_edit_menu']);
    Route::post('admin/update/{id}',[AdminController::class,'post_edit_menu']);
    Route::get('admin/delete/{id}',[AdminController::class,'get_delete_menu']);
    Route::post('multiple_delete_menu',[AdminController::class,'multiple_delete_menu']);
    // Menu Type
    Route::get('/menu_type',[MenuTypeController::class,'index']);
    Route::get('get_menu_type',[MenuTypeController::class,'show']);
    Route::post('menu_type/create_menu_type',[MenuTypeController::class,'insert']);
    Route::get('menu_type/edit/{id}',[MenuTypeController::class,'get_edit_menu_type']);
    Route::post('menu_type/update/{id}',[MenuTypeController::class,'post_edit_menu_type']);
    Route::get('menu_type/delete/{id}',[MenuTypeController::class,'get_delete_post']);
    // Admin Post
    Route::get('/post',[PostController::class,'index']);
    Route::get('create_post',[PostController::class,'show']);
    Route::post('post/create_post',[PostController::class,'insert']);
    Route::get('post/edit/{id}',[PostController::class,'get_edit_post']);
    Route::post('post/update/{id}',[PostController::class,'post_edit_post']);
    Route::get('post/delete/{id}',[PostController::class,'get_delete_post']);
    //Category
    Route::get('/category',[CategoryController::class,'index']);
    Route::get('create_category',[CategoryController::class,'show']);
    Route::post('category/create_category',[CategoryController::class,'insert']);
    Route::get('category/edit/{id}',[CategoryController::class,'get_edit_category']);
    Route::post('category/update/{id}',[CategoryController::class,'post_edit_category']);
    Route::get('category/delete/{id}',[CategoryController::class,'get_delete_category']);
    //Setting
    Route::get('/setting',[ControllerSetting::class,'index']);
    Route::get('setting/create_setting',[ControllerSetting::class,'show']);
    Route::post('setting/create_setting',[ControllerSetting::class,'insert']);
    Route::get('setting/edit/{id}',[ControllerSetting::class,'get_edit_setting']);
    Route::post('setting/update/{id}',[ControllerSetting::class,'post_edit_setting']);
    Route::get('setting/delete/{id}',[ControllerSetting::class,'get_delete_setting']);

    //User
    Route::get('user',[UserController::class,'index']);
    Route::get('create_user',[UserController::class,'show']);  
    Route::post('user/create_user',[UserController::class,'insert']);
    Route::get('user/edit/{id}',[UserController::class,'get_edit_user']);
    Route::post('user/update/{id}',[UserController::class,'post_edit_user']);
    Route::get('user/delete/{id}',[UserController::class,'get_delete_user']);
});
    
//Login
Route::get('login',[LoginController::class,'get_login']);
Route::post('login',[LoginController::class,'post_login']);
Route::get('logout',[LoginController::class,'dologout']);
//Upload Image
Route::post('form',[PostController::class,'upl_image']);
//Delete Image
Route::post('deleteImg',[PostContoller::class,'deleteImg']);

Route::get('/', function () {
    return view('welcome');
});
//Index
Route::get('{url}',[homecontroller::class,'index']);
//Home
Route::get('/home',[MenuController::class,'show']);