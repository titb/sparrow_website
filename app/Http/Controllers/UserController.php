<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class UserController extends Controller
{
    public function index()
    {
        $users=User::get();
        return view('admin.user.user',compact('users'));
    }
    public function show()
    {
        $users=User::orderBy('id','asc')->get();
        return view('admin.user.user_add',compact('users'));
    }
    public function insert(Request $request){
        $this->validate($request,[
    		'username'=>'required|unique:users',
    		// 'permission'=>'required',
    		'password'=>'required',
    		'comfirm_password'=>'required|same:password'
    	]);

        if($request->hasFile('image')){
            $images = $request->file('image');
            $destinationPath = "images/";
            $fileName = $images->getClientOriginalName();
            $fileupload = $images->move($destinationPath,$fileName);
        }else{
             $fileName ="";
        }
    	$data = [
            'name' => $request->name,
    		'username' => $request->username,
            'image' => $fileName,
            'branch_id' =>$request->branch,
    		'email' => $request->email,
            'phone' => $request->phone,
    		'status' => $request->status,
    		'password' => bcrypt($request->password),
    	];

    	$user_id = User::insertGetId($data);

    		// if($user_id != 0){
    		// 	foreach ($request->permission as $key => $value) {
    		// 		$data_ur = [
    		// 		 'user_id' => $user_id,
    		// 		 'role_id' => $request->permission[$key]
    		// 		];
    		// 		DB::table('user_roles')->insert($data_ur);
    		// 	}
    		// }

    		return redirect('create_user')->with('success','Successfull Create');
    }
    public function get_edit_user($id){
       $user=User::where('id','=',$id)->first();
       return view('admin.user.user_edit',compact('user'));
    }
    
    public function post_edit_user(Request $request,$id){
        $user = User::find($id);

    	$this->validate($request,[
    		'username'=>'required',
    	]);

        if($request->hasFile('image')){
            $images = $request->file('image');
            $destinationPath = "images/";
            $fileName = $images->getClientOriginalName();
            $fileupload = $images->move($destinationPath,$fileName);
        }else{
            $fileName=$request->image_hidden;
        }
        
    	$data = [
            'name' => $request->name,
            'username' => $request->username,
            'image' => $fileName,
            'email' => $request->email,
            'branch_id' =>$request->branch,
            'phone' => $request->phone,
            'status' => $request->status,
        ];

    	$user_id = User::where('id','=',$id)->update($data);
    return redirect('user')->with('success','Successfull Update');
    }
    
    public function get_delete_user($id){
        User::where('id','=',$id)->delete();
    return redirect()->to('user')->with('success','Successful Delete');
    }
}
