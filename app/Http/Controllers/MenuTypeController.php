<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu_type;
class MenuTypeController extends Controller
{
    public function index(){
        $menu_type=Menu_type::get();
        return view('admin.menu_type.menu_type',compact('menu_type'));
    }
    public function show(){
        return view('admin.menu_type.menu_type_add');
    }
    public function insert(Request $request)
    {
        $this->validate($request, [
    		'name' => 'required|unique:menu_types'
    	]);
    	$data = [

    		'name' => $request->name,
    		'created_at'=> date('Y-m-d')
    	];
        Menu_type::insert($data);
    
    	return redirect()->back()->with('success','Successful Create');
    }
    public function get_edit_menu_type($id)
    {
    	$menu_type = Menu_type::find($id);
    	return view('admin.menu_type.menu_type_edit',compact('menu_type'));
    }
    public function post_edit_menu_type(Request $request,$id)
    {
    	$this->validate($request,[
    		'name' => 'required|unique:menu_types'
    	]);
    	$data = [
    		'name' => $request->name,
    		'updated_at' => date('Y-m-d')
    	];
    	Menu_type::where('id','=',$id)->update($data);
    	return redirect()->to('menu_type')->with('success','Successful Update');	
    }
    public function get_delete_post($id){
        Menu_type::where('id','=',$id)->delete();
        return redirect()->to('menu_type')->with('success','Sucessful Delete');
    }
}
