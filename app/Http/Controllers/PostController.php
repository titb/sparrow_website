<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Sub_post;
use App\Models\Category;
use DB;
class PostController extends Controller
{
    public function index(){
        $category = Category::where('category_type','=','category')->get();
        $post = Post::orderBy('id','asc')->get();
        return view('admin.post.post',compact('post','category'));
    }
    public function show(){
       $category = Category::where('category_type','=','category')->get();
        $post = Post::orderBy('id','asc')->get();
        return view('admin.post.post_add',compact('post','category'));
    }
    public function insert(Request $request){
        $this->validate($request,[
    		'title' => 'required'
      ]);
      //show title
      if($request->show_title==1){
        $value=1;
      }else{
          $value=0;
      } 

      // if($request->link != Null){
      //   $link = str_slug($request->link ,'-');
      // }else{
      //   $link = str_slug($request->title,'-');
      // }
    	if($request->hasFile('image')){
            $images = $request->file('image');
            $destinationPath = "images/";
            $fileNames = $images->getClientOriginalName();
            $fileName = str_replace(" ","-",$fileNames);
            $fileupload = $images->move($destinationPath,$fileName);
        }else{
             $fileName ="";
        }
        $postcout = Post::get();
        if(count($postcout) > 0){
          $pos_last_id = collect(Post::orderBy('id','asc')->get())->last();
          $id_post = $pos_last_id->id + 1;
        }else{
          $id_post = 1;
        }
        $post = [
            'id' => $id_post,
            'title' => $request->title,
            'action_title'=>$value,
            'show_image_feature'=>$request->show_image_feature==1?1:0,
            'template'=>$request->template,
            // 'link' => $link,
            // 'position' => $request->position,
            'link_download' =>$request->link_download,
            'location_job' =>$request->location_job,
            'language'=>$request->language,
            'description' => $request->description,
            'status' => $request->status,
            'publish_date' => date("Y-m-d h:i:s", strtotime($request->publish_date)),
            'post_type' => $request->post_type,
            'image' => $fileName,
            // 'user_id' => Auth::user()->id,
            'unpublish_date' => date("Y-m-d h:i:s", strtotime($request->unpublish_date)),
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ];
        
        $post_id = Post::insertGetId($post);
            if($post_id != 0 && $request->sub_post_id)
            {
                foreach($request->sub_post_id as $sp)
              {
                $sub_post=[
                  'post_id' => $post_id,
                  'sub_post_id' =>$sp,
                ];
                DB::table('sub_post')->insert($sub_post); 
              } 
            }
            if($request->hasFile('image_multi')) {
              foreach ($request->file('image_multi') as $ke => $value) {
                    $images = $request->file('image_multi')[$ke];
                    $destinationPath = "Galleries/";
                    $fileName = $images->getClientOriginalName();
                    $fileupload = $images->move($destinationPath,$fileName);
                    $post_image = [
                        'post_id' => $post_id,
                        'image' => $request->get('Galleries/',$fileName)
                    ]; 
                    DB::table('post_image')->insert($post_image);
              }
            } 

        return redirect()->to('post')->with('success','Successful Create');
    }
    public function get_edit_post($id)
    {
      $category =  Category::where('category_type','=','category')->get();
    	$posts = Post::orderBy('id','asc')->get();
      $post = Post::where('id','=',$id)->first();
      $post_tr = Post::where('language','!=',$post->language)->get();
      $sub_post=Sub_post::get();
    	return view('admin.post.post_edit',compact('post','posts','post_tr','category','sub_post'));
      
    }
   public function post_edit_post(Request $request,$id)
   {
    $this->validate($request,[
        'title' => 'required'
  ]);
  //show title
  if($request->show_title==1){
    $value=1;
  }else{
    $value=0;
  }
//   if($request->link != Null){
//     $link = str_slug($request->link ,'-');
//   }else{
//     $link = str_slug($request->title,'-');
//   }
  $posts = Post::find($id);
  if($request->hasFile('image')){
      $images = $request->file('image');
      $destinationPath = "images/";
      $fileNames = $images->getClientOriginalName();
      $fileName = str_replace(" ","-",$fileNames);
      $fileupload = $images->move($destinationPath,$fileName);
  }else{
       $fileName =$request->image_hidden;
  }

  $post = [
      'title' => $request->title,
      'action_title'=>$value,
      'show_image_feature'=>$request->show_image_feature==1?1:0,
      'template'=>$request->template,
      // 'link' => $request->link,
      // 'position' => $request->position,
      'link_download' =>$request->link_download,
      'location_job' =>$request->location_job,
      'language'=>$request->language,
      'description' => $request->description,
      'status' => $request->status,
      'publish_date' => date("Y-m-d h:i:s", strtotime($request->publish_date)),
      'post_type' => $request->post_type,
      'image' => $fileName,
    //   'user_id' => Auth::user()->id,
      'unpublish_date' => date("Y-m-d h:i:s", strtotime($request->unpublish_date)),
      // 'created_at' => date('Y-m-d'),
      'updated_at' => date('y-m-d')
    ];

  $post_id = Post::where('id','=',$id)->update($post);
  if($request->sub_post_id)
  {
    DB::table('sub_post')->where('post_id','=',$id)->delete();
      foreach($request->sub_post_id as $sp)
    {
      $sub_post=[
        'post_id' => $id,
        'sub_post_id' =>$sp,
      ];
      DB::table('sub_post')->insert($sub_post); 
    } 
  }
    if($request->hasFile('image_multi')){
      DB::table('post_image')->where('post_id','=',$id)->delete();
        foreach ($request->file('image_multi') as $ke => $value) {
                $images = $request->file('image_multi')[$ke];
                $destinationPath = "Galleries/";
                $fileName = $images->getClientOriginalName();
                $fileupload = $images->move($destinationPath,$fileName);
              $post_image = [
                    'post_id' => $id,
                    'image' => $request->get('Galleries/',$fileName)
              ];

              DB::table('post_image')->insert($post_image);
    }
    }else{
        if($request->mult_image_hidden == 0){
          //   dd("else");
            DB::table('post_image')->where('post_id','=',$id)->delete();
        }
    }
  
    return redirect()->to('post')->with('success','Updated Successful');
   }
   public function get_delete_post($id)
   {
       Post::where('id','=',$id)->delete();
       return redirect()->to('post')->with('success','Successful Delete');
   }
   public function upl_image(Request $request){
    if($request->hasfile('filename'))
    {
       foreach($request->file('filename') as $image)
       {
           $name=$image->getClientOriginalName();
           $new_img_name = rand(123456,999999)."_".$name;
           $image->move(public_path().'/images/', $new_img_name);  
           $data[] = $new_img_name;  
       }
    } 
    return redirect()->to('create_post')->with('success', 'Your images has been successfully');
  }
  public function deleteImg($id) {
    $name=$image->getClientOriginalName();
    if($request->hasFile('filename')){
      $path = storage_path('app/public').'/images/1.jpg';
        if(File::exists($path)){
            unlink($path);
        }
    }
  }
}
