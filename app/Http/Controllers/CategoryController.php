<?php

namespace App\Http\Controllers;
use App\Models\Category;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $category = Category::where('category_type','=','category')->get();
    	return view('admin.category.category',compact('category'));
    }
    public function show(){
        $category = Category::where('category_type','=','category')->get();
    	return view('admin.category.category_add',compact('category'));
    }
    public function insert(Request $request){
        
    	$this->validate($request,[
    		'name' => 'required|unique:category'
    		]);
    	$category = [
            'name' => $request->name,
            // 'language'=>$request->language,
    		'description' => $request->description,
            'block' => $request->block,
            'category_type' => 'category',
            'parent_id' => $request->parent_id,
            'status' => $request->status,
            // 'user_id' => Auth::user()->id,
            'publish_date' => $request->publish_date,
            'unpublish_date' => $request->unpublish_date,
            'created_at' => date('Y-m-d'),
    		];

        Category::insert($category);

        return redirect()->to('create_category')->with('success','Successful Create');
    }
    public function get_edit_category($id)
    {
        $cat = Category::find($id);
        $cate=Category::where('category_type','=','category')->get();
        $category = Category::where('category_type','=','category')->get();
        return view('admin.category.category_edit',compact('category','cat','cate'));
    }
    public function post_edit_category(Request $request,$id)
    {
       $this->validate($request,[
            'name' => 'required'
            ]);

        $category = [
            'name' => $request->name,
            'language'=>$request->language,
            'description' => $request->description,
            'block' => $request->block,
            'category_type' => 'category',
            'parent_id' => $request->parent_id,
            'status' => $request->status,
            // 'user_id' => Auth::user()->id,
            'publish_date' => $request->publish_date,
            'unpublish_date' => $request->unpublish_date,
            'updated_at' => date('Y-m-d'),
            ];

        $cat = Category::where('id','=',$id)->update($category);

        return redirect()->to('category')->with('success','Updated Successful');
    }
    public function get_delete_category($id)
    {
        $cat = Category::find($id);
        $cat->delete();
        return redirect('category')->with('success', 'Deleted Successful');
    }
}
