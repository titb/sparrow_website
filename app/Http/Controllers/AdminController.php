<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Menu_type;
use App\Models\Post;
use App\Models\menu_post;
use DB;
use Illuminate\Support\Str;
class AdminController extends Controller
{
    public function index(){
        $menu_type = Menu_type::all();
        $menu=Menu::orderBy('id','ASC')->get();
        return view('admin.menu.menu',compact('menu','menu_type'));
    }
    public function show(){
        $post=Post::get();
        $menu_type = Menu_type::all();
        $menu=Menu::where('parent_id','=',0)->orderBy('id','asc')->get();
        return view('admin.menu.addmenu',compact('menu','post','menu_type'));
    }
    public function insert(Request $request)
    {
        $this->validate($request,[
    		'name' => 'required'
    	]);
      if($request->link != Null){
        $link = $request->link;
      }else{
        $link = Str::slug($request->name,'-');
      }
      if($request->translation)$link = $request->translation;

    	if($request->hasFile('image')){
            $images = $request->file('image');
            $destinationPath = "images/";
            $fileNames = $images->getClientOriginalName();
            $fileName = str_replace(" ","-",$fileNames);
            $fileupload = $images->move($destinationPath,$fileNames);
        }else{
             $fileName ="";
        }
      $menu = Menu::get();
      if($request->ordering != Null){
        $order = $request->ordering + 0.01;
      }else{
        $order = count($menu);
      }
      if(count($menu) > 0){
        $menu_last_id = collect(Menu::orderBy('id','asc')->get())->last();
        $id_menu = $menu_last_id->id + 1;
      }else{
        $id_menu = 1;
      }
      if($request->show_banner==1){
        $banner=1;
      }else{
          $banner=0;
      } 
        $data = [
          'id' => $id_menu,
          'name' => $request->name,
      	  'parent_id' => $request->parent,
          'show_banner'=>$banner,
          'link' => $link,
          'language' => $request->language,
          'status' => $request->status,
          'modul_class' => $request->modul_class,
      		'image' => $fileName,
      		'menu_type_id' => $request->menu_type,
        //   'user_id' => Auth::user()->id,
          'ordering' => $order,
      		'publish_date' => date('Y-m-d',strtotime($request->publish_date)),
      		'unpublish_date' => date('Y-m-d',strtotime($request->unpublish_date)),
          'deleted' => 1,
      		'created_at' => date('Y-m-d')
      	  ];
  $menu_id = Menu::insertGetId($data);
    
    if($menu_id != 0 && $request->post_id)
    {
        foreach($request->post_id as $p=>$position)
      {
        $post_pos=[
          'menu_id' => $menu_id,
          'post_id' => $request->post_id[$p],
          'position'=>$request->position[$p]
          
        ];
        DB::table('menu_post')->insert($post_pos); 
      }
        // $post_menu =[
        //   'menu_id' => $menu_id,
        //   'post_id' => $p,
        // ];
        // DB::table('menu_post')->insert($post_menu); 
    }
         return redirect()->to('create_menu')->with('success','Successful Create');
    }
    public function get_edit_menu($id)
    {
        
        $menu_type = Menu_type::all();
        $parent=Menu::where('parent_id','=',0)->orderBy('id','asc')->get();
        $menu=Menu::with(['menu_posts'=>function($q){ $q->orderBy('menu_post.position','asc'); }])->where('id','=',$id)->first();
        $post=Post::get();
       return view('admin.menu.menuedit',compact('menu','parent','post','menu_type'));
    }
    public function post_edit_menu(Request $request,$id){
        $menu = Menu::find($id);
        
        if($request->hasFile('image')){
            $images = $request->file('image');
            $destinationPath = "images/";
            $fileNames = $images->getClientOriginalName();
            $fileName = str_replace(" ","-",preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '', $fileNames));
            $fileupload = $images->move($destinationPath,$fileName);
        }else{
             $fileName =$request->image_hidden;
        }
        if($request->ordering != $menu->ordering){
        //   $order = $request->ordering + 0.01;
        $lang = $request->language;
          $count_menu = Menu::where('language',$lang)->orderBy('ordering','asc')->get();
          $count = count($count_menu);
          // $order = $request->ordering + 0.01;
          $order = $request->ordering;
          foreach ($count_menu as $key => $value) {
            $menus_id = Menu::find($value->id);
            $first_order = Menu::where('language',$lang)->orderBy('ordering','asc')->get()->first();
            $last_order = Menu::where('language',$lang)->orderBy('ordering','asc')->get()->last();

            if ($menu->id == $first_order->id) {
              if ($value->ordering <= $request->ordering) {
                // $order = $request->ordering;
                $up_order = [
                              'ordering'=>$menus_id->ordering - 1,
                            ];
                DB::table('menus')->where('id','=',$menus_id->id)->update($up_order);
              }
            }elseif($request->ordering == $last_order->ordering){
              if ($value->ordering > $menu->ordering && $value->ordering <= $request->ordering) {
                // $order = $request->ordering;
                $up_order = [
                              'ordering'=>$menus_id->ordering - 1,
                            ];
                DB::table('menus')->where('id','=',$menus_id->id)->update($up_order);
              }
            }else{
              // $order = $request->ordering;
              // $value->ordering > $menu->ordering && $value->ordering <= $request->ordering
              if ($request->ordering < $menu->ordering) {
                if ($value->ordering > $request->ordering && $value->ordering < $menu->ordering) {
                  $order = $request->ordering + 1;
                  $up_order = [
                                'ordering'=>$menus_id->ordering + 1,
                              ];
                //   DB::table('menus')->where('id','=',$menus_id->id)->update($up_order);            
                }else{
                  $order = $request->ordering + 1;
                }
              }else{
                if ($value->ordering > $menu->ordering && $value->ordering <= $request->ordering) {
                  $up_order = [
                                'ordering'=>$menus_id->ordering - 1,
                              ];
                  DB::table('menus')->where('id','=',$menus_id->id)->update($up_order);
                }
              }
            }
          }
        }else{
          $order = $request->ordering;
        }
        if($request->link != Null){
          $link = $request->link ;
        }else{
          $link = Str::slug($request->name,'-');
        }
        if($request->show_banner==1){
          $banner=1;
        }else{
          $banner=0;
        }
        $data = [
            'name' => $request->name,
            'parent_id' => $request->parent,
            'show_banner'=>$banner,
            'link' => $link,
            
            'language' => $request->language,
            'status' => $request->status,
            'image' => $fileName,
            'menu_type_id' => $request->menu_type,
            'modul_class' => $request->modul_class,
            // 'user_id' => Auth::user()->id,
            'ordering' => $order,
            'publish_date' => date('Y-m-d',strtotime($request->publish_date)),
            'unpublish_date' => date('Y-m-d',strtotime($request->unpublish_date)),
            'updated_at' => date('Y-m-d'),
            ];
        $menu_id=Menu::where('id','=',$id)->update($data);
        if($request->post_id)
      {
        DB::table('menu_post')->where('menu_id','=',$id)->delete();
        foreach($request->post_id as $p=>$position)
        {
          $post_pos=[
            'menu_id'=>$id,
            'post_id' => $request->post_id[$p],
            'position'=>$request->position[$p]
          ];
          DB::table('menu_post')->insert($post_pos); 
        }
      }
        return redirect()->to('admin')->with('success','Successful Edit');
   }
    public function get_delete_menu($id){
        Menu::where('id','=',$id)->delete();
        return redirect()->to('admin')->with('success','Successful Delete');
    }
}
