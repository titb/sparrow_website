<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Models\Menu;
use\App\Models\Setting;

class homecontroller extends Controller
{
    public function index(Request $request,$url)
    {
        $setting=Setting::orderBy('id','asc')->first();
        $Menu=Menu::where('parent_id','=',0)->orderBy('id','asc')->get();
        $menu=Menu::where('link','=',$url)->where('parent_id','=',0)->orderBy('id','asc')->first();
        return view('index',compact('Menu','menu','setting'));
    }
}
