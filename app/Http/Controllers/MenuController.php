<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
class MenuController extends Controller
{
    public function show(){
        $menu=Menu::where('parent_id','=',0)->orderBy('id','asc')->get();
        return view('home.home',compact('menu'));
    }
}
