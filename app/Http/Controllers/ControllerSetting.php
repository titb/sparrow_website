<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;

class ControllerSetting extends Controller
{
    public function index(){
        $setting=Setting::get();
        return view('admin.setting.setting',compact('setting'));
    }
    public function show(){
        $setting=Setting::get();
        return view('admin.setting.setting_add',compact('setting'));
    }
    public function insert(Request $request)
    {
        $this->validate($request,[
    		'website_name' => 'required'
    	]);

    	if($request->hasFile('logo_image')){
            $logo_image = $request->file('logo_image');
            $destinationPath = "images/";
            $fileNameLogos = $logo_image->getClientOriginalName();
            $fileNameLogo = str_replace(" ","-",$fileNameLogos);
            $fileupload = $logo_image->move($destinationPath,$fileNameLogos);
        }else{
            $fileNameLogo ="";
        }
        if($request->hasFile('favicon_image')){
            $favicon_image = $request->file('favicon_image');
            $destinationPath = "images/";
            $fileFavicons  = $favicon_image->getClientOriginalName();
            $fileFavicon = str_replace(" ","-",$fileFavicons);
            $fileupload = $favicon_image->move($destinationPath,$fileFavicons);
        }else{
            $fileFavicon ="";
        }
      $setting = Setting::get();
      if($request->is_slide_only_page==1){
        $only_page=1;
      }else{
          $only_page=0;
      } 
        $data = [
          'website_name' => $request->website_name,
          'website_url' => $request->website_url,
      	  'language' => $request->language,
          'is_slide_only_page'=>$only_page,
          'phone' => $request->phone,
          'email'=>$request->email,
          'language' => $request->language,
          'logo_image'=>$fileNameLogo,
          'logo_text'=>$request->text_logo,
          'favicon_image'=> $fileFavicon,
          'work_time' => $request->work_time,
          'copyright' => $request->copyright,
          'link_fb' => $request->link_fb,
          'address' => $request->address,
          'address_site' => $request->address_site,
        //   'user_id' => Auth::user()->id,
      		'created_at' => date('Y-m-d')
      	  ];
        $setting = Setting::insertGetId($data);
    
         return redirect()->to('setting/create_setting')->with('success','Successful Create');
    }
    public function get_edit_setting($id)
    {
      $setting=Setting::where('id','=',$id)->first();
      return view('admin.setting.setting_edit',compact('setting'));
    }
    public function post_edit_setting(Request $request,$id){
      $this->validate($request,[
    		'website_name' => 'required'
    	]);
      $setting = Setting::find($id);
    	if($request->hasFile('logo_image')){
            $logo_image = $request->file('logo_image');
            $destinationPath = "images/";
            $fileNameLogos = $logo_image->getClientOriginalName();
            $fileNameLogo = str_replace(" ","-",$fileNameLogos);
            $fileupload = $logo_image->move($destinationPath,$fileNameLogos);
        }else{
          $fileNameLogo =$request->image_hidden;
        }
        if($request->hasFile('favicon_image')){
            $favicon_image = $request->file('favicon_image');
            $destinationPath = "images/";
            $fileFavicons  = $favicon_image->getClientOriginalName();
            $fileFavicon = str_replace(" ","-",$fileFavicons);
            $fileupload = $favicon_image->move($destinationPath,$fileFavicons);
        }else{
          $fileFavicon =$request->fav_image_hidden;
        }
      $setting = Setting::get();
      if($request->is_slide_only_page==1){
        $only_page=1;
      }else{
          $only_page=0;
      } 
        $data = [
          'website_name' => $request->website_name,
          'website_url' => $request->website_url,
      	  'language' => $request->language,
          'is_slide_only_page'=>$only_page,
          'phone' => $request->phone,
          'email'=>$request->email,
          'language' => $request->language,
          'logo_image'=>$fileNameLogo,
          'logo_text'=>$request->text_logo,
          'favicon_image'=> $fileFavicon,
          'work_time' => $request->work_time,
          'copyright' => $request->copyright,
          'link_fb' => $request->link_fb,
          'address' => $request->address,
          'address_site' => $request->address_site,
        //   'user_id' => Auth::user()->id,
      		'created_at' => date('Y-m-d')
      	  ];
        Setting::where('id','=',$id)->update($data);
      return redirect()->to('setting')->with('success','Updated Successful');
    }

  public function get_delete_setting($id){
      Setting::where('id','=',$id)->delete();
    return redirect()->to('setting')->with('success','Successful Delete');
  }
}
