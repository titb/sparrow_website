<?php

namespace App\Models;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $table='menus';
    public function menu_type(){
    	return $this->belongsTo(Menu_type::class,'menu_type');
    }
    public function menu_posts(){
        return $this->belongsToMany(Post::class,'menu_post','menu_id','post_id')->withPivot('position');
    }
}
