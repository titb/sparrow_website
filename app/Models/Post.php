<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Postimage;
use App\Models\Sub_post;
class Post extends Model
{
    use HasFactory;
    protected $table='posts';
    public function image_post()
    {
        return $this->hasMany(Postimage::class,'post_id');
    }
    public function sub_posts()
    {
        return $this->belongsToMany(Post::class,'sub_post','post_id','sub_post_id');
    }
}
